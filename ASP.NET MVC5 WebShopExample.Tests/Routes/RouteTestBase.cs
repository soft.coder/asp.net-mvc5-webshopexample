﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace ASP.NET_MVC5_WebShopExample.Tests.Routes
{
    [TestClass]
    public abstract class RouteTestBase
    {
        public MockRouteBuilder MockRouteBuilder { get; private set; }
        public RouteCollection Routes { get; private set; }
        public string Controller { get; protected set; }
        public string Action { get; protected set; }
        public RouteValueDictionary ExpectedRouteValues { get; private set; }

        [TestInitialize]
        public virtual void TestInitialize()
        {
            Controller = "ProductImages";
            Action = "upload";
            MockRouteBuilder = new MockRouteBuilder();
            Routes = new RouteCollection();
            RouteConfig.RegisterRoutes(Routes);
        }

        protected virtual string GenerateUrl()

            => $"/{Controller}/{Action}";

        [TestMethod]
        public virtual void IncomingUrl_WithAllRouteValues_ShouldReturnRouteData()
        {
            string url = GenerateUrl();

            var routeData = Routes.GetRouteData(MockRouteBuilder.BuildHttpContextBase($"~{url}"));

            Assert.IsNotNull(routeData);
            var expectedRouteValues = CreateRouteValuesExpectedFromIncomingUrl();
            AssertRoute.AreEqualRouteValueDictionaries(routeData.Values, expectedRouteValues);
        }
        [TestMethod]
        public virtual void OutgoingUrl_WithAllRouteValues_ShouldReturnUrl()
        {
            MockRouteBuilder.BuildHttpContextBase();
            MockRouteBuilder.BuildRequestContext();
            RouteValueDictionary routeValues = CreateRouteValueDictionaryWithAllValues();

            string outgoingUrl = UrlHelper.GenerateUrl(GetRouteName(), Action,
                Controller, routeValues, Routes, MockRouteBuilder.RequestContext, true);

            Assert.AreEqual(outgoingUrl, GenerateUrl());
        }

        protected virtual RouteValueDictionary CreateRouteValuesExpectedFromIncomingUrl()
        {
            return new RouteValueDictionary()
            {
                ["action"] = Action,
                ["controller"] = Controller,
            };
        }

        protected virtual RouteValueDictionary CreateRouteValueDictionaryWithAllValues()
        {
            return new RouteValueDictionary()
            {
                { "action", Action },
                { "controller", Controller },
            };
        }
        protected abstract string GetRouteName();

    }
}
