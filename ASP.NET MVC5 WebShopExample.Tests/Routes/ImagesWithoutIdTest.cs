﻿using ASP.NET_MVC5_WebShopExample.Constants;
using ASP.NET_MVC5_WebShopExample.Models.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace ASP.NET_MVC5_WebShopExample.Tests.Routes
{
    [TestClass]
    public class ImagesWithoutIdTest: RouteTestBase
    {
        public int OwnerId { get; protected set; }

        [TestInitialize]
        public override void TestInitialize()
        {
            base.TestInitialize();
            OwnerId = 4;
        }
        protected override string GenerateUrl()
        {
            return $"{base.GenerateUrl()}/owner-id/{OwnerId}";
        }

        protected override RouteValueDictionary CreateRouteValuesExpectedFromIncomingUrl()
        {
            var expectedRouteValues = base.CreateRouteValuesExpectedFromIncomingUrl();
            expectedRouteValues.Add("ownerId", OwnerId.ToString());
            return expectedRouteValues;
        }

        protected override RouteValueDictionary CreateRouteValueDictionaryWithAllValues()
        {
            var routeValues = base.CreateRouteValueDictionaryWithAllValues();
            routeValues.Add("ownerId", OwnerId);
            return routeValues;
        }

        protected override string GetRouteName()
        {
            return RouteNames.ImagesWithoutId;
        }


    }
}
