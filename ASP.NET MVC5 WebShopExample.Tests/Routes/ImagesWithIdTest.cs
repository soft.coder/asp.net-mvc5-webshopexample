﻿using ASP.NET_MVC5_WebShopExample.Constants;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace ASP.NET_MVC5_WebShopExample.Tests.Routes
{
    [TestClass]
    public class ImagesWithIdTest: ImagesWithoutIdTest
    {
        public int ImageId { get; private set; }

        [TestInitialize]
        public override void TestInitialize()
        {
            base.TestInitialize();
            ImageId = 1;
            Action = "Edit";
        }

        protected override string GenerateUrl()

            => $"{base.GenerateUrl()}/id/{ImageId}";

        protected override RouteValueDictionary CreateRouteValuesExpectedFromIncomingUrl()
        {
            var expectedRouteValues = base.CreateRouteValuesExpectedFromIncomingUrl();
            expectedRouteValues.Add("id", ImageId.ToString());
            return expectedRouteValues;
        }

        protected override RouteValueDictionary CreateRouteValueDictionaryWithAllValues()
        {
            var routeValueDictionary = base.CreateRouteValueDictionaryWithAllValues();
            routeValueDictionary.Add("id", ImageId);
            return routeValueDictionary;
        }
        protected override string GetRouteName()
        {
            return RouteNames.ImagesWithId;
        }
    }
}
