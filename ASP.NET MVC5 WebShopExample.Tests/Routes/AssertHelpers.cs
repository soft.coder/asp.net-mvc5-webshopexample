﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace ASP.NET_MVC5_WebShopExample.Tests.Routes
{
    public static class AssertRoute
    {
        public static void AreEqualRouteValueDictionaries(RouteValueDictionary routeValues, RouteValueDictionary expectedRouteValues)
        {
            Assert.AreEqual(routeValues.Values.Count, expectedRouteValues.Values.Count);
            Assert.AreEqual(routeValues.Keys.Count, expectedRouteValues.Keys.Count);
            var result = routeValues.All(r => 
                expectedRouteValues.ContainsKey(r.Key) && 
                expectedRouteValues[r.Key].ToString() == r.Value.ToString());
            Assert.IsTrue(result);
        }
    }
}
