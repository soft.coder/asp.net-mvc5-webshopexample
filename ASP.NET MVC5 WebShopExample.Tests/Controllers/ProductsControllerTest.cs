﻿using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using ASP.NET_MVC5_WebShopExample.Controllers;
using ASP.NET_MVC5_WebShopExample.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Net;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.Collections;
using MockBuilder;
using System.Diagnostics;
using ASP.NET_MVC5_WebShopExample.Models.ViewModels;

namespace ASP.NET_MVC5_WebShopExample.Tests.Controllers
{
    [TestClass]
    public class ProductsControllerTest
    {
        public Product TestProduct { get; private set; }
        public IList<Product> ProductList { get; private set; }
        public IList<Category> CategoryList { get; private set; }

        private BuilderDirector<ProductsController> BuilderDirectorCreate(string userId, DateTime now)
        {
            var builderDirector = new BuilderDirector<ProductsController>();
            builderDirector.BuildDbContext();
            builderDirector.BuildDbSet<Product>().Setup(dsp => dsp.Add(It.IsAny<Product>()));
            builderDirector.BuildDbSet<Category>();

            builderDirector.BuildService<ICurrentUser>().Setup(cu => cu.GetUserId()).Returns(userId);
            builderDirector.BuildService<ICurrentDateTime>().Setup(cd => cd.Get()).Returns(now);
            builderDirector.BuildDbSetsInDbContext();
            builderDirector.BuildController(() => 
                new ProductsController(
                    builderDirector.DbContext, 
                    builderDirector.GetServiceMock<ICurrentDateTime>().Object));
            return builderDirector;
        }
        private BuilderDirector<ProductsController> BuilderDirectorCreate()
        {
            return BuilderDirectorCreate(string.Empty, DateTime.Now);
        }
        private BuilderMocker<ProductsController> BuilderMockerCreate(BuilderDirector<ProductsController> mb)
        {
            return new BuilderMocker<ProductsController>(mb);
        }

        private (BuilderDirector<ProductsController> builderDirector,
            BuilderMocker<ProductsController> builderMocker)
            BuildMocksForEditAndCreateActions(string userId = null,
                DateTime? now = null, IEnumerable<Category> categories = null)
        {
            var builderDirector = new BuilderDirector<ProductsController>();
            var builderMocker = new BuilderMocker<ProductsController>(builderDirector);
            builderDirector.BuildDbContext();

            var mockCurrentDateTime = builderDirector.BuildService<ICurrentDateTime>();
            if (now.HasValue)
            {
                mockCurrentDateTime
                    .Setup(cdt => cdt.Get())
                    .Returns(now.Value);
            }
            if (userId != null)
            {
                builderDirector.BuildService<ICurrentUser>()
                    .Setup(cu => cu.GetUserId())
                    .Returns("userId");
            }
            if (categories != null)
            {
                builderDirector.BuildDbSet<Category>();
                builderMocker.MockDbSetAsyncQuery(categories);
            }
            builderDirector.BuildDbSet<Product>()
                .Setup(ds => ds.Add(It.IsAny<Product>()));
            builderMocker.MockSaveChangeAsync();
            builderDirector.BuildDbSetsInDbContext();
            builderDirector.BuildController(() =>
                new ProductsController(
                    builderDirector.DbContext,
                    builderDirector.GetServiceMock<ICurrentDateTime>().Object));
            return (builderDirector, builderMocker);
        }
        private void CompareSelectListWithList<TItem>(SelectList selectList, IList<TItem> listItems) where TItem : class
        {
            var countItems = 0;
            foreach (var item in selectList.Items)
                countItems++;
            Assert.AreEqual(countItems, listItems.Count);
            foreach (var itemOfList in listItems)
            {
                bool isFound = false;
                foreach (var itemOfSelectList in selectList.Items)
                {
                    if (itemOfSelectList is TItem itemOfSelectListCasted)
                        if (object.ReferenceEquals(itemOfList, itemOfSelectListCasted))
                            isFound = true;
                }
                Assert.IsTrue(isFound);
            }
        }

        [TestInitialize]
        public void TestInit()
        {
            TestProduct = new Product { ProductId = 1, Name = "TestProduct", Cost = 12 };
            ProductList = new List<Product>()
            {
                new Product{ ProductId = 1, Name = "Product1"},
                new Product{ ProductId = 2, Name = "Product2"}
            };
            CategoryList = new List<Category>()
            {
                new Category{ CategoryId = 1, Name = "Catregory1" },
                new Category{ CategoryId = 2, Name = "Category2" }
            };
        }



        [TestMethod]
        public async Task Index_WithoutArgs_ShouldReturnViewWithProdcutsCategoriesAndUser()
        {

            var productList = new List<Product>
            {
                new Product{ ProductId = 1, Name = "Product1"},
                new Product{ ProductId = 2, Name = "Product2"}
            };
            var bd = new BuilderDirector<ProductsController>();
            var bm = BuilderMockerCreate(bd);
            bd.BuildDbContext();
            bd.BuildDbSet<Product>().Setup(dsp => dsp.Add(It.IsAny<Product>()));
            bd.BuildService<ICurrentDateTime>().Setup(cd => cd.Get()).Returns(DateTime.Now);
            bm.MockDbSetAsyncQuery(productList);
            bm.MockIncludeString<Product>(nameof(TestProduct.Category));
            var mockProduct = bm.MockIncludeString<Product>(nameof(TestProduct.CreatedByUser));
            bd.BuildDbSetsInDbContext();
            bd.BuildController(() => new ProductsController(bd.DbContext, bd.GetServiceMock<ICurrentDateTime>().Object));

            var result = await bd.Controller.Index();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            mockProduct.Verify();
            Assert.IsInstanceOfType(viewResult.Model, typeof(IEnumerable<Product>));
            var modelProductList = viewResult.Model as IEnumerable<Product>;
            foreach (var modelProduct in modelProductList)
            {
                var isFound = false;
                foreach (var product in productList)
                {
                    if (product == modelProduct)
                    {
                        isFound = true;
                        break;
                    }

                }
                Assert.IsTrue(isFound);
            }
        }

        [TestMethod]
        public async Task DeletePost_WithProductId_ShouldDeleteProductAndRedirect()
        {
            const int id = 1;
            var bd = BuilderDirectorCreate();
            var bm = BuilderMockerCreate(bd);
            var mockDbSet = bm.MockFindAsync(id, TestProduct);
            mockDbSet
                .Setup(ds => ds.Remove(
                    It.Is<Product>(removeProduct => removeProduct == TestProduct)
                )).Verifiable($"Should delete product with id:{id} and name:{TestProduct.Name}");

            var result = await bd.Controller.DeleteConfirmed(id);

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            var redirectToRouteResult = result as RedirectToRouteResult;
            Assert.AreEqual(redirectToRouteResult.RouteValues["action"], "Index");
            bd.DbContextMock.Verify(dc => dc.SaveChangesAsync(), Times.Once);
            mockDbSet.Verify();
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task DeletePost_WithInvalidProductId_ShouldThrowException()
        {
            const int id = 2;
            var bd = BuilderDirectorCreate();
            var bm = BuilderMockerCreate(bd);
            var mockDbSet = bm.MockFindAsync<Product>(id, null);
            mockDbSet.Setup(ds => ds.Remove(It.Is<Product>(p => p == null))).Throws(new ArgumentNullException());

            var result = await bd.Controller.DeleteConfirmed(id);

            Assert.Fail("Should throw exception");
        }
        [TestMethod]
        public async Task DeleteGet_WithoutArgs_ShouldReturnHttpBadRequest()
        {
            int? id = null;
            var mb = BuilderDirectorCreate();

            var result = await mb.Controller.Delete(id);

            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var statusCodeResult = result as HttpStatusCodeResult;
            Assert.AreEqual(statusCodeResult.StatusCode, (int)HttpStatusCode.BadRequest);
        }
        [TestMethod]
        public async Task DeleteGet_WithProductId_ShouldReturnViewResult()
        {
            const int id = 1;
            var bd = BuilderDirectorCreate();
            var bm = BuilderMockerCreate(bd);
            var mockDbSet = bm.MockFindAsync(id, TestProduct);

            var result = await bd.Controller.Delete(id);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.AreEqual(viewResult.Model, TestProduct);
            mockDbSet.Verify();
        }
        [TestMethod]
        public async Task DeleteGet_WithInvalidProductId_ShouldReturnHttpNotFound()
        {
            const int id = 2;
            var bd = BuilderDirectorCreate();
            var bm = BuilderMockerCreate(bd);
            var mockDbSet = bm.MockFindAsync<Product>(id, null);

            var result = await bd.Controller.Delete(id);

            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
            mockDbSet.Verify();
        }
        [TestMethod]
        public async Task Details_WithoutArgs_ShouldReturnHttpBadRequest()
        {
            int? id = null;
            var bd = BuilderDirectorCreate();

            var result = await bd.Controller.Details(id);

            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var statusCodeResult = result as HttpStatusCodeResult;
            Assert.AreEqual(statusCodeResult.StatusCode, (int)HttpStatusCode.BadRequest);
        }
        [TestMethod]
        public async Task Details_WithProductId_ShouldReturnViewResult()
        {
            const int id = 1;
            var bd = BuilderDirectorCreate();
            var bm = BuilderMockerCreate(bd);
            var mockDbSet = bm.MockFindAsync(id, TestProduct);

            var result = await bd.Controller.Details(id);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.AreEqual(viewResult.Model, TestProduct);
            mockDbSet.Verify();
        }
        [TestMethod]
        public async Task Details_WithInvalidProductId_ShouldReturnHttpNotFound()
        {
            const int id = 2;
            var bd = BuilderDirectorCreate();
            var bm = BuilderMockerCreate(bd);
            var mockDbSet = bm.MockFindAsync<Product>(id, null);

            var result = await bd.Controller.Details(id);

            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
            mockDbSet.Verify();
        }
        private void MockTryUpdateModel(ProductsController controller, string newProductName)
        {
            var mockRequestContext = new Mock<RequestContext>();
            var controllerContext = new ControllerContext(mockRequestContext.Object, controller);
            controller.ControllerContext = controllerContext;
            var prefix = "Product";
            FormCollection fc = new FormCollection()
            {
                { $"{prefix}.{nameof(TestProduct.ProductId)}", TestProduct.ProductId.ToString() },
                { $"{prefix}.{nameof(TestProduct.Name)}", newProductName },
                { $"{prefix}.{nameof(TestProduct.Cost)}", TestProduct.Cost.ToString() }
            };
            controller.ValueProvider = fc.ToValueProvider();
        }
        [TestMethod]
        public async Task EditPost_WithProduct_ShouldSaveProduct()
        {
            var entityStateMock = new Mock<DbEntityEntry<Product>>();
            var now = DateTime.Now;
            var oldProductName = TestProduct.Name;
            var newProductName = TestProduct.Name + "_changed";
            var (bd, bm) = BuildMocksForEditAndCreateActions(now: now);
            var mockDbSet = bm.MockFindAsync(TestProduct.ProductId, TestProduct);
            MockTryUpdateModel(bd.Controller, newProductName);


            var result = await bd.Controller.EditPost(TestProduct.ProductId);

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            var redirectResult = result as RedirectToRouteResult;
            Assert.AreEqual(redirectResult.RouteValues["action"], "Index");
            Assert.AreEqual(TestProduct.Name, newProductName);
            bd.DbContextMock.Verify();
            bd.DbContextMock.Verify(dc => dc.SaveChangesAsync(), Times.Once);
            mockDbSet.Verify();
        }
        [TestMethod]
        public async Task EditPost_WithInvalidProduct_ShouldReturnViewWithErrros()
        {
            var now = DateTime.Now;
            var newProductName = TestProduct.Name + "_changed";
            var (bd, bm) = BuildMocksForEditAndCreateActions
                (now: now, categories: CategoryList);
            var productImage = new Image() { ImageId = 1 };
            TestProduct.Images = new List<Image>() { productImage };
            bm.MockFindAsync(TestProduct.ProductId, TestProduct);
            MockTryUpdateModel(bd.Controller, newProductName);
            const string ErrorMessage = "TestErrorMessage";
            bd.Controller.ModelState.AddModelError(string.Empty, ErrorMessage);

            var result = await bd.Controller.EditPost(TestProduct.ProductId);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.IsInstanceOfType(viewResult.Model, typeof(ProductEditViewModel));
            var viewModel = viewResult.Model as ProductEditViewModel;
            Assert.AreEqual(viewModel.Product, TestProduct);
            Assert.AreEqual(viewModel.Product.Name, newProductName);
            Assert.AreEqual(viewModel.Images.Count(), TestProduct.Images.Count);
            Assert.AreEqual(viewModel.Images.First().ImageId, TestProduct.Images.First().ImageId);
            CompareSelectListWithList(viewModel.SelectListOfCategories, CategoryList);
        }

        [TestMethod]
        public async Task EditGet_WithId_ShouldReturnViewWithProduct()
        {
            int? id = 1;
            var (bd, bm) = BuildMocksForEditAndCreateActions(categories: CategoryList);
            var productImage = new Image() { ImageId = 1 };
            TestProduct.Images = new List<Image>() { productImage };
            var mockDbSet = bm.MockFindAsync(id.Value, TestProduct);

            var result = await bd.Controller.Edit(id);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.IsInstanceOfType(viewResult.Model, typeof(ProductEditViewModel));
            var viewModel = viewResult.Model as ProductEditViewModel;
            Assert.AreEqual(viewModel.Product, TestProduct);
            Assert.AreEqual(viewModel.Images.Count(), TestProduct.Images.Count);
            Assert.AreEqual(viewModel.Images.First().ImageId, TestProduct.Images.First().ImageId);
            CompareSelectListWithList(viewModel.SelectListOfCategories, CategoryList);
            mockDbSet.Verify();
        }
        [TestMethod]
        public async Task EditGet_WithoutArgs_ShouldReturnHttpBadRequest()
        {
            var bd = BuilderDirectorCreate();
            int? id = null;

            var result = await bd.Controller.Edit(id);

            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var statusCodeResult = result as HttpStatusCodeResult;
            Assert.AreEqual(statusCodeResult.StatusCode, (int)HttpStatusCode.BadRequest);
        }
        [TestMethod]
        public async Task EditGet_WithInvalidId_ShouldReturnHttpNotFound()
        {
            const int id = 2;
            var (bd, bm) = BuildMocksForEditAndCreateActions();
            var mockDbSet = bm.MockFindAsync<Product>(id, null);

            var result = await bd.Controller.Edit(id);

            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
            mockDbSet.Verify();
        }



        [TestMethod]
        public async Task CreateGet_WithoutArgs_ShouldReturnView()
        {
            var (bd, _) = BuildMocksForEditAndCreateActions(categories: CategoryList);
            
            var result = await bd.Controller.Create() as ViewResult;

            Assert.IsInstanceOfType(result.Model, typeof(ProductCreateViewModel));
            var viewModel = result.Model as ProductCreateViewModel;
            Assert.IsInstanceOfType(viewModel.SelectListOfCategories, typeof(SelectList));
            CompareSelectListWithList(viewModel.SelectListOfCategories, CategoryList);
            Assert.IsInstanceOfType(viewModel.Product, typeof(Product));
            bd.DbContextMock.Verify(dc => dc.Categories, Times.Once);
        }
        [TestMethod]
        public async Task CreatePost_WithProduct_ShouldCreateProduct()
        {
            const string userId = "userId";
            DateTime now = DateTime.Now;
            var (bd, bm) = BuildMocksForEditAndCreateActions(userId, now);
            

            var result = await bd.Controller.Create(bd.GetServiceMock<ICurrentUser>().Object, TestProduct);


            bd.GetMockDbSet<Product>().Verify(m => m.Add(It.Is<Product>(p =>
                p.ProductId == TestProduct.ProductId &&
                p.Name == TestProduct.Name &&
                p.Cost == TestProduct.Cost &&
                p.CreatedByUserId == userId &&
                p.CreatedDate == now
            )));
            bd.DbContextMock.Verify(c => c.SaveChangesAsync(), Times.Once());
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            var redirectResult = result as RedirectToRouteResult;
            Assert.AreEqual(redirectResult.RouteValues["action"], "CreateSpecifications");
            Assert.AreEqual(redirectResult.RouteValues["id"], TestProduct.ProductId);
        }
        [TestMethod]
        public async Task CreatePost_WithInvailidProduct_ShouldReturnViewWithErrors()
        {
            const string errorMessage = "errorMessage";
            DateTime now = DateTime.Now;
            const string userId = "userId";
            var (bd, _) = BuildMocksForEditAndCreateActions(userId, now, CategoryList);
            bd.Controller.ModelState.AddModelError(string.Empty, errorMessage);

            var result = await bd.Controller.Create(bd.GetServiceMock<ICurrentUser>().Object, TestProduct);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.IsInstanceOfType(viewResult.Model, typeof(ProductCreateViewModel));
            var viewModel = viewResult.Model as ProductCreateViewModel;
            Assert.AreEqual(viewModel.Product, TestProduct);
            CompareSelectListWithList(viewModel.SelectListOfCategories, CategoryList);
        }
        public async Task TestMockController()
        {
            var mockDbContext = new Mock<ApplicationDbContext>();
            var mockCurrentTime = new Mock<ICurrentDateTime>();
            var requestContext = new Mock<RequestContext>();
            var controller = new ProductsController(mockDbContext.Object, mockCurrentTime.Object);
            var controllerContext = new ControllerContext(requestContext.Object, controller);
            controller.ControllerContext = controllerContext;
            const string userName = "UserName";
            requestContext.Setup(rc => rc.HttpContext.User.Identity.Name).Returns(userName);

            //var viewResult = await controller.TestMockControllerContext() as ViewResult;
            //Assert.IsInstanceOfType(viewResult.Model, typeof(string));
            //Assert.AreEqual(viewResult.Model, userName);
        }
    }
}
