﻿using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using ASP.NET_MVC5_WebShopExample.Controllers;
using ASP.NET_MVC5_WebShopExample.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockBuilder;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.Tests.Controllers
{
    [TestClass]
    public class CategoriesControllerTest
    {
        public BuilderDirector<CategoriesController> BD { get; protected set; }
        public WebShopExampleBuilderMocker<CategoriesController> BM { get; protected set; }
        public Mock<ApplicationDbContext> MockDbContext { get; protected set; }
        public List<Category> Categories { get; protected set; }

        [TestInitialize]
        public void TestInit()
        {
            BD = new BuilderDirector<CategoriesController>();
            BM = new WebShopExampleBuilderMocker<CategoriesController>(BD);
            MockDbContext = BD.BuildDbContext();
            Categories = new List<Category>
            {
                new Category { CategoryId = 1, Name = "Category1" },
                new Category { CategoryId = 2, Name = "Category2" },
                new Category { CategoryId = 3, Name = "Category3" },
                new Category { CategoryId = 4, Name = "Category4" },
                new Category { CategoryId = 5, Name = "Category5" },
            };
        }

        protected List<Category> SetupIndex()
        {
            var c = new Category();
            var categories = new List<Category>
            {
                new Category {CategoryId = 1, Name = "Category1"},
                new Category {CategoryId = 2, Name = "Category2"},
                new Category {CategoryId = 3, Name = "Category3"}
            };
            BD.BuildDbSet<Category>();
            BM.MockDbSetAsyncQuery(categories);
            BM.MockIncludeString<Category>(nameof(c.ParentCategory));
            BM.MockIncludeString<Category>(nameof(c.CreatedByUser));
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContextMock.Object));
            return categories;
        }


        [TestMethod]
        public async Task Index_WithoutArgs_ShouldReturnCategories()
        {
            var categories = SetupIndex();

            var result = await BD.Controller.Index();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.IsInstanceOfType(viewResult.Model, typeof(IEnumerable<Category>));
            var viewCategories = viewResult.Model as IEnumerable<Category>;
            int i = 0;
            foreach (var viewCategory in viewCategories)
            {
                Assert.AreEqual(viewCategory, categories[i++]);
            }
            var mockCategoryDbSet = BD.GetMockDbSet<Category>();
            mockCategoryDbSet.Verify();
        }

        public Category SetupDetils()
        {
            var category = new Category { CategoryId = 1, Name = "Category1" };
            BD.BuildDbSet<Category>();
            BM.MockFindAsync(category.CategoryId, category);
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));
            return category;
        }

        [TestMethod]
        public async Task Details_WithCategoryId_ShouldReturnCategory()
        {
            var category = SetupDetils();

            var result = await BD.Controller.Details(category.CategoryId);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.AreEqual(viewResult.Model, category);
            var mockCategoryDbSet = BD.GetMockDbSet<Category>();
            mockCategoryDbSet.Verify();
        }
        [TestMethod]
        public async Task Details_WithInvalidCategoryId_ShouldReturnNotFound()
        {
            SetupDetils();

            var result = await BD.Controller.Details(2);

            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
        }
        [TestMethod]
        public async Task Details_WithoutNullCategoryId_ShouldReturnBadRequest()
        {
            SetupDetils();

            var result = await BD.Controller.Details(null);

            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var codeResult = result as HttpStatusCodeResult;
            Assert.AreEqual(codeResult.StatusCode, (int)HttpStatusCode.BadRequest);
        }

        public IList<Category> SetupCreate()
        {
            var categories = new List<Category>
            {
                new Category { CategoryId = 1, Name = "Category1"},
                new Category { CategoryId = 2, Name = "Category2"},
                new Category { CategoryId = 3, Name = "Category3"}
            };
            BD.BuildDbSet<Category>();
            BM.MockDbSetQuery<Category>(categories);
            return categories;
        }
        public void VerifyCreatePostSelectList(SelectList selectList, IList<Category> categories)
        {
            int i = 0;
            foreach (var item in selectList)
            {
                Assert.AreEqual(item.Value, categories[i].CategoryId.ToString());
                Assert.AreEqual(item.Text, categories[i++].Name);
            }

        }

        [TestMethod]
        public void Create_WithoutArgs_ShouldReturnViewWithCategoriesAvailableAsParent()
        {
            var parentCategories = SetupCreate();
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = BD.Controller.Create();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.IsInstanceOfType(viewResult.ViewBag.ParentCategories, typeof(SelectList));
            var selectList = viewResult.ViewBag.ParentCategories as SelectList;
            VerifyCreatePostSelectList(selectList, parentCategories);
        }
        [TestMethod]
        public async Task CreatePost_WithICurrentUserAndCategory_ShouldSaveCategoryAndRedirectToCategoriesIndex()
        {
            
            var categories = SetupCreate();
            var category = new Category { CategoryId = 3, Name = "Category3" };
            const string userId = "userId";
            var currentUser = BD.BuildService<ICurrentUser>();
            currentUser.Setup(cu => cu.GetUserId())
                .Returns(userId)
                .Verifiable("Should get id of current user");
            var mockCategoryDbSet = BD.GetMockDbSet<Category>();
            mockCategoryDbSet
                .Setup(ds => ds.Add(It.Is<Category>(c => c == category)))
                .Verifiable("Should add category to DbSet<Category>");
            MockDbContext
                .Setup(dc => dc.SaveChangesAsync())
                .Returns(() => Task.FromResult(1))
                .Verifiable("Should call SaveChangeAsync");
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));
            var result = await BD.Controller.Create(currentUser.Object, category);
            
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            var redirectResult = result as RedirectToRouteResult;
            Assert.AreEqual(redirectResult.RouteValues["action"], "Index");
            currentUser.Verify();
            mockCategoryDbSet.Verify();
            MockDbContext.Verify();
        }
        [TestMethod]
        public async Task CreatePost_WithICurrentUserAndInvalidCategory_ShouldReturnInvalidCategoryWithCategoriesAvailableAsParent()
        {
            var parentCategories = SetupCreate();
            var category = new Category { CategoryId = 3, Name = "Category3" };
            const string userId = "userId";
            var currentUser = BD.BuildService<ICurrentUser>();
            currentUser.Setup(cu => cu.GetUserId())
                .Returns(userId)
                .Verifiable("Should get id of current user");
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));
            const string errorMessage = "errorMsg";
            BD.Controller.ModelState.AddModelError("", errorMessage);

            var result = await BD.Controller.Create(currentUser.Object, category);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            currentUser.Verify();
            Assert.AreEqual(viewResult.Model, category);

            Assert.IsInstanceOfType(viewResult.ViewBag.ParentCategories, typeof(SelectList));
            var selectList = viewResult.ViewBag.ParentCategories as SelectList;

            VerifyCreatePostSelectList(selectList, parentCategories);
        }

        public void VerifyEditParentSelectList(SelectList selectList, IList<Category> categories, Category excludeCategory)
        {
            foreach (var category in categories)
            {
                bool isFound = false;
                foreach (var item in selectList)
                {
                    if (category.CategoryId.ToString() == item.Value)
                    {
                        isFound = true;
                        Assert.AreEqual(item.Value, category.CategoryId.ToString());
                        Assert.IsTrue(item.Text.Contains(category.Name));
                        Assert.IsTrue(item.Text.Contains(category.CategoryId.ToString()));
                    }
                }
                if (isFound == false && category.CategoryId != excludeCategory.CategoryId)
                    Assert.Fail($"Not found category:{category.CategoryId}");
                else if (isFound == true && category.CategoryId == excludeCategory.CategoryId)
                    Assert.Fail("Should exclude edited category from categories available as parent");
            }
        }

        [TestMethod]
        public async Task Edit_WithCategoryId_ShouldReturnCategoryWithCategoriesAvailableAsParent()
        {
            var category = Categories[2];
            BD.BuildDbSet<Category>();
            BM.MockDbSetQuery(Categories);
            var mockCategoryDbSet = BM.MockFindAsync(category.CategoryId, category);
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = await BD.Controller.Edit(category.CategoryId);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.AreEqual(viewResult.Model, category);
            Assert.IsInstanceOfType(viewResult.ViewBag.ParentCategories, typeof(SelectList));
            var selectList = viewResult.ViewBag.ParentCategories;
            VerifyEditParentSelectList(selectList, Categories, category);
            mockCategoryDbSet.Verify();
        }
        [TestMethod]
        public async Task Edit_WithNullCategoryId_ShoulReturnHttpBadRequestStatusCode()
        {
            var mockCategoryDbSet = BD.BuildDbSet<Category>();
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = await BD.Controller.Edit(null);

            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var statusCodeResult = result as HttpStatusCodeResult;
            Assert.AreEqual(statusCodeResult.StatusCode, (int)HttpStatusCode.BadRequest);
        }
        [TestMethod]
        public async Task Edit_WithInvalidCategoryId_ShoulReturnNotFoundResult()
        {
            var category = Categories[Categories.Count - 1];
            BD.BuildDbSet<Category>();
            var mockCategoryDbSet = BM.MockFindAsync(category.CategoryId, category);
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            int foundId = category.CategoryId + 1;
            var result = await BD.Controller.Edit(foundId);

            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
            mockCategoryDbSet
                .Verify(ds => ds.FindAsync(It.Is<int>(id => id == foundId)), Times.Once);
            mockCategoryDbSet
                .Verify(ds => ds.FindAsync(It.Is<int>(id => id == category.CategoryId)), Times.Never);
        }
        public (Category newCategory,
            Category oldCategory,
            Mock<DbSet<Category>>)
            SetupEditPost(bool isValid = true, bool isChanged = true)
        {
            var newCategory = new Category
            {
                CategoryId = 1,
                Name = "Category1",
                ParentCategoryId = 2,
                CreatedByUserId = "createdUserId"
            };
            Category oldCategory;
            if (isChanged)
            {
                oldCategory = new Category
                {
                    CategoryId = newCategory.CategoryId,
                    Name = "Old" + newCategory.Name,
                    ParentCategoryId = newCategory.ParentCategoryId
                };
            }
            else
            {
                oldCategory = new Category
                {
                    CategoryId = newCategory.CategoryId,
                    Name = newCategory.Name,
                    ParentCategoryId = newCategory.ParentCategoryId,
                };
            }
            var mockCategoryDbSet = BD.BuildDbSet<Category>();
            BM.MockFindAsync(newCategory.CategoryId, oldCategory);
            return (newCategory, oldCategory, mockCategoryDbSet);
        }
        [TestMethod]
        public async Task EditPost_WithCategory_ShouldSaveCategoryAndUpdateCategoryHistoryAndRedirectToIndexAction()
        {
            var (newCategory, oldCategory, mockDbSet) = SetupEditPost();
            
            var currentTime = DateTime.Now;
            var mockCurrentTime = BM.MockGetCurrentDateTime(currentTime);
            const string updatedUserId = "userId";
            var mockCurrentUser = BM.MockGetCurrentUserId(updatedUserId);
            
            var mockCategoryUpdateHistoryDbSet = BD.BuildDbSet<CategoryUpdateHistory>();
            mockCategoryUpdateHistoryDbSet
                .Setup(ds => ds.Add(It.Is<CategoryUpdateHistory>(cuh =>
                        cuh.CurrentCategoryId == newCategory.CategoryId &&
                        cuh.UpdateDate == currentTime &&
                        cuh.UpdatedByUserId == updatedUserId &&
                        cuh.Name == newCategory.Name &&
                        cuh.ParentCategoryId == null &&
                        cuh.CreatedByUserId == null
                ))).Verifiable("Should add update history of categories");
            
            BM.MockChangeEntryState(newCategory, EntityState.Modified);
            BM.MockSaveChangeAsync();
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = await BD.Controller.Edit(mockCurrentTime.Object, 
                mockCurrentUser.Object, newCategory);

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            var redirectResult = result as RedirectToRouteResult;
            Assert.AreEqual(redirectResult.RouteValues["action"], "Index");

            mockCurrentTime.Verify();
            mockCurrentUser.Verify();
            mockDbSet.Verify();
            mockCategoryUpdateHistoryDbSet.Verify();
            BD.DbContextMock.Verify();
        }
        [TestMethod]
        public async Task EditPost_WithInvalidCategory_ShouldReturnViewResult()
        {
            var (newCategory, oldCategory, mockCategoryDbSet) = SetupEditPost(isValid: false);
            BM.MockDbSetQuery(Categories);
            var mockCurrentDateTime = BM.MockGetCurrentDateTime();
            var mockCurrentUser = BM.MockGetCurrentUserId();
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));
            BD.Controller.ModelState.AddModelError("", "errorMessage");

            var result = await BD.Controller.Edit(mockCurrentDateTime.Object,
                mockCurrentUser.Object, newCategory);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.AreEqual(viewResult.Model, newCategory);
            Assert.IsInstanceOfType(viewResult.ViewBag.ParentCategories, typeof(SelectList));
            var selectList = viewResult.ViewBag.ParentCategories;
            VerifyEditParentSelectList(selectList, Categories, newCategory);
            mockCategoryDbSet.Verify();
        }
        [TestMethod]
        public async Task EditPost_WithUnchangedCategory_ShoulReturnViewResult()
        {
            var (newCategory, oldCategory, mockCategoryDbSet) = SetupEditPost(isChanged: false);
            BM.MockDbSetQuery(Categories);
            var mockCurrentDateTime = BM.MockGetCurrentDateTime();
            var mockCurrentUser = BM.MockGetCurrentUserId();
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = await BD.Controller.Edit(mockCurrentDateTime.Object,
                mockCurrentUser.Object, newCategory);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.AreEqual(viewResult.Model, newCategory);
            Assert.IsInstanceOfType(viewResult.ViewBag.ParentCategories, typeof(SelectList));
            var selectList = viewResult.ViewBag.ParentCategories;
            VerifyEditParentSelectList(selectList, Categories, newCategory);
            mockCategoryDbSet.Verify();
            Assert.IsFalse(oldCategory.IsChanged(newCategory));
        }
        [TestMethod]
        public async Task Delete_WithNullId_ShouldReturnBadRequest()
        {
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = await BD.Controller.Delete(null);

            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var statusResult = result as HttpStatusCodeResult;
            Assert.AreEqual(statusResult.StatusCode, (int)HttpStatusCode.BadRequest);
        }
        [TestMethod]
        public async Task Delete_WithNotExistedId_ShouldReturnHttpNotFound()
        {
            const int id = 1;
            var mockDbSet = BD.BuildDbSet<Category>();
            BM.MockFindAsync<Category>(id: id, returnEntity: null);
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = await BD.Controller.Delete(id);

            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
            mockDbSet.Verify();
        }
        [TestMethod]
        public async Task Delete_WithId_ShouldReturnViewResult()
        {
            var category = Categories[0];
            var mockDbSet = BD.BuildDbSet<Category>();
            BM.MockFindAsync(id: category.CategoryId, returnEntity: category);
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = await BD.Controller.Delete(category.CategoryId);

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.AreEqual(viewResult.Model, category);
            mockDbSet.Verify();
        }
        [TestMethod]
        public async Task DeletePost_WithId_ShouldDeleteAndRedirectToAction()
        {
            var category = Categories[0];
            var mockCategoryDbSet = BD.BuildDbSet<Category>();
            BM.MockFindAsync(category.CategoryId, returnEntity: category);
            BM.MockRemove(category);
            BM.MockSaveChangeAsync();
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = await BD.Controller.DeleteConfirmed(category.CategoryId);

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            var redirectResult = result as RedirectToRouteResult;
            Assert.AreEqual(redirectResult.RouteValues["action"], "Index");
            mockCategoryDbSet.Verify();
            BD.DbContextMock.Verify();
        }
        [TestMethod]
        public async Task DeletePost_WithNotExistedId_ShouldThrowException()
        {
            const int id = 1;
            var mockDbSet = BD.BuildDbSet<Category>();
            BM.MockFindAsync<Category>(id, returnEntity: null);
            BD.BuildDbSetsInDbContext();
            BD.BuildController(() => new CategoriesController(BD.DbContext));

            var result = await BD.Controller.DeleteConfirmed(id);

            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            var redirectResult = result as RedirectToRouteResult;
            Assert.AreEqual(redirectResult.RouteValues["action"], "Index");
            mockDbSet.Verify();
        }
    }
}
