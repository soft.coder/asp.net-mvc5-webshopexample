﻿using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using ASP.NET_MVC5_WebShopExample.Models;
using MockBuilder;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.Tests
{
    public class WebShopExampleBuilderMocker<TController> : BuilderMocker<TController> where TController : Controller
    {
        public WebShopExampleBuilderMocker(BuilderDirector<TController> mockControllerBuilder) : base(mockControllerBuilder)
        { }

        public Mock<ICurrentUser> MockGetCurrentUserId(string userId = "userId", bool isVerfiy = true)
        {
            var currentUserService = BuilderDirector.BuildService<ICurrentUser>();
            var mockFlow = currentUserService
                .Setup(cu => cu.GetUserId())
                .Returns(userId);
            if (isVerfiy)
                mockFlow.Verifiable("Should get user id of current user");
            return currentUserService;
        }
        public Mock<ICurrentDateTime> MockGetCurrentDateTime(DateTime dateTime, bool isVerify = true)
        {
            if (dateTime == null)
                dateTime = DateTime.Now;
            var currentDateTimeService = BuilderDirector.BuildService<ICurrentDateTime>();
            var mockFlow = currentDateTimeService
                .Setup(cdt => cdt.Get())
                .Returns(dateTime);
            if (isVerify)
                mockFlow.Verifiable("Should get current date time");
            return currentDateTimeService;
        }
        public Mock<ICurrentDateTime> MockGetCurrentDateTime(bool isVerify = true)
        {
            return MockGetCurrentDateTime(DateTime.Now, isVerify);
        }
        public void MockChangeEntryState<TEntity>(TEntity entity, EntityState entityState, bool isVerify = true) where TEntity: class
        {
            var mockFlow = BuilderDirector.DbContextMock
                .Setup(dc => dc.ChangeEntryState(
                    It.Is<TEntity>(c => c == entity),
                    It.Is<EntityState>(es => es == entityState)
                    ));
            if(isVerify)
            {
                string state = Enum.GetName(typeof(EntityState), entityState);
                string entityType = typeof(TEntity).Name;
                mockFlow.Verifiable($"Should change '{entityType}' entity state to '{state}'");
            }
        }
    }
}
