﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockBuilder
{
    public class MockControllerBuilderException : Exception 
    {
        public MockControllerBuilderException()
        { }
        public MockControllerBuilderException(string message) : base(message)
        { }
        public MockControllerBuilderException(string message, Exception inner): base(message, inner)
        { }
    }
    public class DbContextShouldBuildBeforeDbSets : MockControllerBuilderException { }
    public class DbContextShouldContainAppropriateDbSet : MockControllerBuilderException { }
    public class UnExpectedExceptionWhenMockDbSetInDbContext : MockControllerBuilderException 
    {
        public UnExpectedExceptionWhenMockDbSetInDbContext(Exception inner): base(string.Empty, inner)
        { }
    }
    public class NotFoundParameterOfControllerConstructor : MockControllerBuilderException
    {
        public NotFoundParameterOfControllerConstructor(Type type, string name)
        {
            Type = type;
            Name = name;
        }

        public Type Type { get; }
        public string Name { get; }
    }
    public class NotFoundAppropriateControllerConstructor : MockControllerBuilderException { }
    public class NotFoundDbSetMock : MockControllerBuilderException { }
    public class NotFoundServiceMock : MockControllerBuilderException { }
}
