﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockBuilder
{
    public interface IMockDbSetBuilder
    {
        Mock BuildDbSet();
        void BuildDbSetInDbContext();
        Mock GetMockDbSet();
    }
}
