﻿using ASP.NET_MVC5_WebShopExample.Models;
using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Reflection;

namespace MockBuilder
{
    public class MockControllerBuilder<TController> where TController: Controller
    {

        protected ArrayList ServiceMocks { get; set; } = new ArrayList();
        public TController Controller { get; protected set; }

        public Mock<T> BuildService<T>() where T: class
        {
            var mockService = new Mock<T>();
            ServiceMocks.Add(mockService);
            return mockService;
        }
        public TController BuildController(Func<TController> controllerFactory)
        {
            return Controller = controllerFactory();
        }

        public Mock<T> GetServiceMock<T>() where T: class
        {
            foreach (var serviceMock in ServiceMocks)
            {
                if(serviceMock.GetType() == typeof(Mock<T>))
                {
                    return serviceMock as Mock<T>;
                }
            }
            throw new NotFoundServiceMock();
        }
    }
}
