﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;

namespace MockBuilder
{
    public class MockRouteBuilder
    {
        public Mock<HttpContextBase> MockHttpContextBase { get; protected set; }
        public HttpContextBase HttpContextBase => MockHttpContextBase.Object;

        public RequestContext RequestContext { get; protected set; }

        public HttpContextBase BuildHttpContextBase(string targetUrl = null, string httpMethod = "GET")
        {
            var mockRequest = new Mock<HttpRequestBase>();
            mockRequest.Setup(rb => rb.AppRelativeCurrentExecutionFilePath).Returns(targetUrl);
            mockRequest.Setup(rb => rb.HttpMethod).Returns(httpMethod);

            var mockResponse = new Mock<HttpResponseBase>();
            mockResponse
                .Setup(rb => rb.ApplyAppPathModifier(It.IsAny<string>()))
                .Returns<string>(s => s);

            MockHttpContextBase = new Mock<HttpContextBase>();
            MockHttpContextBase.Setup(c => c.Request).Returns(mockRequest.Object);
            MockHttpContextBase.Setup(c => c.Response).Returns(mockResponse.Object);

            return MockHttpContextBase.Object;
        }
        public RequestContext BuildRequestContext()
        {
            if (MockHttpContextBase == null)
                throw new ArgumentNullException(nameof(MockHttpContextBase), "You should run before `MockRouteBuilder.BuildHttpContextBase()`" );

            return RequestContext = new RequestContext(MockHttpContextBase.Object, new RouteData());
        }
    }
}
