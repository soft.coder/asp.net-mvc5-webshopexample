﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Pluralization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MockBuilder
{
    public class MockDbSetBuilder<TEntity, TDbContext>: IMockDbSetBuilder where TEntity: class where TDbContext: DbContext
    {
        public string NameOfPropertyInDbContext { get; protected set; }
        public Mock<DbSet<TEntity>> MockDbSet { get; protected set; }
        public Mock<TDbContext> MockDbContext { get; protected set; }

        public MockDbSetBuilder(Mock<TDbContext> mockDbContext, string nameOfPropertyInDbContext)
        {
            MockDbContext = mockDbContext;
            NameOfPropertyInDbContext = nameOfPropertyInDbContext;
        }
        public Mock BuildDbSet()
        {
            if (MockDbContext == null)
                throw new DbContextShouldBuildBeforeDbSets();
            MockDbSet = new Mock<DbSet<TEntity>>();

            return MockDbSet;
        }
        public void BuildDbSetInDbContext()
        {
            try
            {
                ParameterExpression param = Expression.Parameter(typeof(TDbContext));
                Expression<Func<TDbContext, DbSet<TEntity>>> lambdaExpression = Expression.Lambda<Func<TDbContext, DbSet<TEntity>>>(
                    Expression.Property(
                        param, typeof(TDbContext), NameOfPropertyInDbContext
                    )
                    , param
                );
                MockDbContext.Setup(lambdaExpression).Returns(MockDbSet.Object);
            } 
            catch(Exception ex)
            {
                string errorMsg = $"Property '{NameOfPropertyInDbContext}' is not defined for type '{typeof(TDbContext).FullName}'";
                if (ex is ArgumentException && ex.Message.Contains(errorMsg))
                    throw new DbContextShouldContainAppropriateDbSet();
                else
                    throw new UnExpectedExceptionWhenMockDbSetInDbContext(ex);
            }
        }
        public Mock GetMockDbSet()
        {
            return MockDbSet;
        }
    }
}
