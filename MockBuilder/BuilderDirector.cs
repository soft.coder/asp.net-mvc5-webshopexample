﻿using ASP.NET_MVC5_WebShopExample.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MockBuilder
{
    public class BuilderDirector<TController> where TController : Controller
    {
        public Mock<ApplicationDbContext> DbContextMock
        {
            get => MockDbContextBuilder.DbContextMock;
        }
        public ApplicationDbContext DbContext => DbContextMock.Object;

        public TController Controller 
        {
            get => MockControllerBuilder.Controller;
        }

        public MockDbContextBuilder<ApplicationDbContext> MockDbContextBuilder { get; } = new MockDbContextBuilder<ApplicationDbContext>();
        public MockControllerBuilder<TController> MockControllerBuilder { get; } = new MockControllerBuilder<TController>();

        public Mock<ApplicationDbContext> BuildDbContext()
        {
            return MockDbContextBuilder.BuildDbContext();
        }
        public Mock<DbSet<T>> BuildDbSet<T>(string nameOfPropertyInDbContext) where T : class
        {
            return MockDbContextBuilder.BuildDbSet<T>(nameOfPropertyInDbContext);
        }
        public Mock<DbSet<T>> BuildDbSet<T>() where T : class
        {
            return MockDbContextBuilder.BuildDbSet<T>();
        }
        public Mock<T> BuildService<T>() where T : class
        {
            return MockControllerBuilder.BuildService<T>();
        }

        public TController BuildController(Func<TController> controllerFactory)
        {
            return MockControllerBuilder.BuildController(controllerFactory);
        }

        public Mock<DbSet<T>> GetMockDbSet<T>() where T : class
        {
            return MockDbContextBuilder.GetMockDbSet<T>();
        }
        public Mock<T> GetServiceMock<T>() where T : class
        {
            return MockControllerBuilder.GetServiceMock<T>();
        }

        public void BuildDbSetsInDbContext()
        {
            MockDbContextBuilder.BuildDbSetsInDbContext();
        }

    }
}
