﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Pluralization;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ASP.NET_MVC5_WebShopExample.Models;
using Moq;

namespace MockBuilder
{
    public class MockDbContextBuilder<TDbContext> where TDbContext: DbContext
    {
        public Mock<TDbContext> DbContextMock { get; protected set; }

        protected IList<IMockDbSetBuilder> DbSetMockBuilders { get; set; } = new List<IMockDbSetBuilder>();

        public Mock<TDbContext> BuildDbContext()
        {
            return DbContextMock = new Mock<TDbContext>();
        }
        public Mock<DbSet<T>> BuildDbSet<T>(IMockDbSetBuilder mockDbSetBuilder) where T : class
        {
            if (DbContextMock == null)
                throw new DbContextShouldBuildBeforeDbSets();
            mockDbSetBuilder.BuildDbSet();
            DbSetMockBuilders.Add(mockDbSetBuilder);

            return mockDbSetBuilder.GetMockDbSet() as Mock<DbSet<T>>;
        }
        public Mock<DbSet<T>> BuildDbSet<T>(string nameOfPropertyInDbContext) where T : class
        {
            if (DbContextMock == null)
                throw new DbContextShouldBuildBeforeDbSets();
            var mockDbSetBuilder = new MockDbSetBuilder<T, TDbContext>(DbContextMock, nameOfPropertyInDbContext);
            return BuildDbSet<T>(mockDbSetBuilder);
        }
        public Mock<DbSet<T>> BuildDbSet<T>() where T: class
        {
            if (DbContextMock == null)
                throw new DbContextShouldBuildBeforeDbSets();
            string typeName = typeof(T).Name;
            var pluralize = new EnglishPluralizationService();
            string propName = pluralize.Pluralize(typeName);
            return BuildDbSet<T>(propName);
        }
        public Mock<DbSet<T>> GetMockDbSet<T>() where T : class
        {
            foreach (var mock in DbSetMockBuilders)
            {
                Mock mockDbSet = mock.GetMockDbSet();
                if (mockDbSet.GetType() == typeof(Mock<DbSet<T>>))
                {
                    return mockDbSet as Mock<DbSet<T>>;
                }
            }
            throw new NotFoundDbSetMock();
        }
        public void BuildDbSetsInDbContext()
        {
            if (DbContextMock == null)
                throw new DbContextShouldBuildBeforeDbSets();
            foreach (IMockDbSetBuilder mockDbSet in DbSetMockBuilders)
            {
                mockDbSet.BuildDbSetInDbContext();
            }
        }
    }
}
