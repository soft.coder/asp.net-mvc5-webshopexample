﻿using Moq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MockBuilder
{
    public class BuilderMocker<TController> where TController: Controller
    {
        public BuilderDirector<TController> BuilderDirector { get; }
        public bool IsGetAsyncEnumeratorCalled { get; private set; }
        public bool IsGenericGetAsyncEnumeratorCalled { get; private set; }

        public BuilderMocker(BuilderDirector<TController> mockControllerBuilder)
        {
            BuilderDirector = mockControllerBuilder;
        }

        public Mock<DbSet<TEntity>> MockDbSetQuery<TEntity>(IEnumerable<TEntity> collection) where TEntity : class
        {
            var queryable = collection.AsQueryable<TEntity>();
            var mockDbSet = BuilderDirector.GetMockDbSet<TEntity>();
            mockDbSet.As<IQueryable<TEntity>>().Setup(m => m.Provider).Returns(queryable.Provider);
            mockDbSet.As<IQueryable<TEntity>>().Setup(m => m.Expression).Returns(queryable.Expression);
            mockDbSet.As<IQueryable<TEntity>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            mockDbSet.As<IQueryable<TEntity>>().Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());
            return mockDbSet;
        }

        public Mock<DbSet<TEntity>> MockDbSetAsyncQuery<TEntity>(IEnumerable<TEntity> collection) where TEntity: class
        {
            var queryable = collection.AsQueryable<TEntity>();
            var mockDbSet = BuilderDirector.GetMockDbSet<TEntity>();

            mockDbSet.As<IQueryable<TEntity>>().Setup(m => m.Expression).Returns(queryable.Expression);
            mockDbSet.As<IQueryable<TEntity>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            mockDbSet.As<IQueryable<TEntity>>().Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());

            mockDbSet.As<IDbAsyncEnumerable<TEntity>>()
               .Setup(m => m.GetAsyncEnumerator())
               .Callback(() => IsGenericGetAsyncEnumeratorCalled = true)
               .Returns(new TestDbAsyncEnumerator<TEntity>(collection.GetEnumerator()));
            
            if(collection is IEnumerable enumerableNonGeneric)
            { 
                mockDbSet.As<IDbAsyncEnumerable>()
                   .Setup(m => m.GetAsyncEnumerator())
                   .Callback(() => IsGetAsyncEnumeratorCalled = true)
                   .Returns(new TestDbAsyncEnumerator(enumerableNonGeneric.GetEnumerator()));

            }

            mockDbSet.As<IQueryable<TEntity>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<TEntity>(queryable.Provider));

            return mockDbSet;
        }

        public Mock<DbSet<TEntity>> MockIncludeString<TEntity>(string path, bool isVerify = true)
            where TEntity : class
        {
            var mockDbSet = BuilderDirector.GetMockDbSet<TEntity>();
            var mocker = mockDbSet
                .Setup(ds => ds.Include(It.Is<string>(pathParam => pathParam == path)))
                .Returns(mockDbSet.Object);
            if (isVerify) 
                mocker.Verifiable();
            return mockDbSet;
        }

        public Mock<DbSet<TEntity>> MockFindAsync<TEntity>(int id, TEntity returnEntity, bool isVerifiable = true) where TEntity : class
        {
            var mockDbSet = BuilderDirector.GetMockDbSet<TEntity>();
            var mockFlow = mockDbSet.Setup(ds => ds.FindAsync(It.Is<int>(findId => findId == id)))
                .Returns(Task.FromResult<TEntity>(returnEntity));
            if(isVerifiable)
                mockFlow.Verifiable($"Should call FindAsync with id:{id}");

            return mockDbSet;
        }

        public void MockSaveChangeAsync(int returnValue = 1, bool isVerify = true)
        {
            var mockFlow = BuilderDirector.DbContextMock
                .Setup(dc => dc.SaveChangesAsync())
                .Returns(Task.FromResult(returnValue));
            if (isVerify)
                mockFlow.Verifiable("Should call saveChangeAsync");
        }
        public Mock<DbSet<TEntity>> MockRemove<TEntity>(TEntity entity) where TEntity: class
        {
            var mockDbSet = BuilderDirector.GetMockDbSet<TEntity>();
            mockDbSet
                .Setup(ds => ds.Remove(
                    It.Is<TEntity>(removeEntity => removeEntity == entity)
                )).Verifiable($"Should delete product");
            return mockDbSet;
        }

    }
}
