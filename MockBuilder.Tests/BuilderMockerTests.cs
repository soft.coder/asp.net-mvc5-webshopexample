﻿using ASP.NET_MVC5_WebShopExample.Controllers;
using ASP.NET_MVC5_WebShopExample.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockBuilder.Tests
{
    [TestClass]
    public class BuilderMockerTests
    {
        protected BuilderDirector<ProductsController> BD { get; set; }
        protected BuilderMocker<ProductsController> BM { get; set; }
        public List<Product> ProductList { get; private set; }

        [TestInitialize]
        public void Init()
        {
            BD = new BuilderDirector<ProductsController>();
            BM = new BuilderMocker<ProductsController>(BD);
            ProductList = new List<Product>()
            {
                new Product{ ProductId = 1},
                new Product{ ProductId = 2}
            };
        }

        [TestMethod]
        public void MockDbSetQuery_WithIEnumerable_ShouldMockDbSet()
        {
            BD.BuildDbContext();
            BD.BuildDbSet<Product>();

            BM.MockDbSetQuery(ProductList);
            BD.BuildDbSetsInDbContext();

            var result = from p in BD.DbContextMock.Object.Products
                         where p.ProductId == 1
                         select p;

            Assert.AreEqual(result.ToList()[0], ProductList[0]);
        }
        [TestMethod]
        public async Task MockDbSetAsyncQuery_WithIEnumarable_ShoulMockDbSetForAsyncQueries()
        {
            BD.BuildDbContext();
            BD.BuildDbSet<Product>();
            BM.MockDbSetAsyncQuery(ProductList);
            BD.BuildDbSetsInDbContext();

            var result = from p in BD.DbContextMock.Object.Products
                         where p.ProductId == 1
                         select p;

            Assert.AreEqual((await result.ToListAsync())[0], ProductList[0]);
        }
        [TestMethod]
        public void MockIncludeString_WithPath_ShouldMockIncludeMethod()
        {
            BD.BuildDbContext();
            var mockDbSet = BD.BuildDbSet<Product>();
            const string categoryPath = "Category";
            BM.MockIncludeString<Product>(categoryPath);
            const string imagesPath = "Images";
            BM.MockIncludeString<Product>(imagesPath);
            BD.BuildDbSetsInDbContext();

            BD.DbContextMock.Object.Products.Include(categoryPath).Include(imagesPath);

            mockDbSet.Verify();
        }
        [TestMethod]
        public async Task MockFindAsync_WithIdAndReturnEntity_ShouldReturnEntity()
        {
            BD.BuildDbContext();
            var mockDbSet = BD.BuildDbSet<Product>();
            BD.BuildDbSetsInDbContext();
            const int id = 1;
            var product = new Product { ProductId = id };
            BM.MockFindAsync(id, product);

            var result = await BD.DbContextMock.Object.Products.FindAsync(id);

            Assert.AreEqual(result, product);
            mockDbSet.Verify();
        }
    }
}
