﻿using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using ASP.NET_MVC5_WebShopExample.Controllers;
using ASP.NET_MVC5_WebShopExample.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using MockBuilder;
using ASP.NET_MVC5_WebShopExample.Models.ViewModels;

namespace MockBuilder.Tests
{
    [TestClass]
    public class BuilderDirectorTest
    {
        private const string ShouldThrowExceptionMsg = "Should throw exception";

        public BuilderDirector<Controller> BD { get; private set; }

        [TestInitialize]
        public void Init()
        {
            BD = new BuilderDirector<Controller>();
        }

        [TestMethod]
        public void Constructor_WithoutArgs_ShouldCreate()
        {
            Assert.IsInstanceOfType(BD, typeof(BuilderDirector<Controller>));
        }
        [TestMethod]
        public void BuildDbContext_WithoutArgs_ShouldCreateDbContextMock()
        {
            var mockDbContext = BD.BuildDbContext();

            Assert.IsInstanceOfType(mockDbContext, typeof(Mock<ApplicationDbContext>));
            Assert.AreEqual(BD.DbContextMock, mockDbContext);
        }
        [TestMethod]
        [ExpectedException(typeof(DbContextShouldBuildBeforeDbSets))]
        public void BuildDbSet_WithoutArgs_ShouldThrowExceptionWhenBuildBeforeDbContext()
        {
            BD.BuildDbSet<Product>();

            Assert.Fail(ShouldThrowExceptionMsg);
        }
        [TestMethod]
        [ExpectedException(typeof(DbContextShouldContainAppropriateDbSet))]
        public void BuildDbSet_WithoutArgsWithoutDbSetInDbContext_ShouldThrowExceptionWithoutDbSetInDbContext()
        {
            BD.BuildDbContext();
            BD.BuildDbSet<CurrentUser>();
            BD.BuildDbSetsInDbContext();

            Assert.Fail(ShouldThrowExceptionMsg);
        }
        [TestMethod]
        public void BuildDbSet_WithoutArgsAfterBuildDbContext_ShouldSuccessBuildDbSetMock()
        {
            BD.BuildDbContext();
            var mockDbSet = BD.BuildDbSet<Product>();

            Assert.IsInstanceOfType(mockDbSet, typeof(Mock<DbSet<Product>>));
        }
        [TestMethod]
        public void BuildService_WithoutArgs_ShouldCreateServiceMock()
        {
            var mockService = BD.BuildService<ICurrentUser>();

            Assert.IsInstanceOfType(mockService, typeof(Mock<ICurrentUser>));
        }
        [TestMethod]
        public void BuildController_ProductsController_ShouldCreateProductsController()
        {
            var bd = new BuilderDirector<ProductsController>();
            bd.BuildDbContext();
            bd.BuildService<ICurrentDateTime>();

            var mockController = bd.BuildController(() =>
                new ProductsController(bd.DbContext, bd.GetServiceMock<ICurrentDateTime>().Object));

            Assert.IsInstanceOfType(mockController, typeof(ProductsController));

        }
        [TestMethod]
        public void BuildControllerMock_ProductsWithDbContextAndService_ShouldCreateMockOfController()
        {
            var mb = new BuilderDirector<ProductsController>();
            var mockDbContext = mb.BuildDbContext();
            var mockCurrentDateTime = mb.BuildService<ICurrentDateTime>();

            var controller = mb.BuildController(() =>
                new ProductsController(mockDbContext.Object, mockCurrentDateTime.Object));

            Assert.IsInstanceOfType(controller, typeof(ProductsController));
        }
        public void CompareSelectListWithList<TItem>(SelectList selectList, IList<TItem> listItems) where TItem : class
        {
            var countItems = 0;
            foreach (var item in selectList.Items)
                countItems++;
            Assert.AreEqual(countItems, listItems.Count);
            foreach (var itemOfList in listItems)
            {
                bool isFound = false;
                foreach (var itemOfSelectList in selectList.Items)
                {
                    if (itemOfSelectList is TItem itemOfSelectListCasted)
                        if (object.ReferenceEquals(itemOfList, itemOfSelectListCasted))
                            isFound = true;
                }
                Assert.IsTrue(isFound);
            }
        }
        [TestMethod]
        public async Task BuildControllerMock_ProductsCallCreate_ShouldCallCreateActionOfController()
        {
            var categories = new List<Category>()
            {
                new Category { CategoryId = 1, Name = "Category1" },
                new Category { CategoryId = 2, Name = "Category2" }
            };
            var bd = new BuilderDirector<ProductsController>();
            var bm = new BuilderMocker<ProductsController>(bd);
            var mockDbContext = bd.BuildDbContext();
            var mockCategoryDbSet = bd.BuildDbSet<Category>();
            bm.MockDbSetAsyncQuery(categories);
            var mockCurrentDateTime = bd.BuildService<ICurrentDateTime>();
            bd.BuildDbSetsInDbContext();
            var controller = bd.BuildController(() =>
                new ProductsController(mockDbContext.Object, mockCurrentDateTime.Object));

            var result = await controller.Create();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = result as ViewResult;
            Assert.IsInstanceOfType(viewResult.Model, typeof(ProductCreateViewModel));
            var viewModel = viewResult.Model as ProductCreateViewModel;
            Assert.IsNotNull(viewModel.Product);
            CompareSelectListWithList(viewModel.SelectListOfCategories, categories);
        }
        [TestMethod]
        [ExpectedException(typeof(NotFoundDbSetMock))]
        public void GetMockDbSet_WithoutApproriateDbSet_ShouldThrowException()
        {
            BD.GetMockDbSet<Product>();

            Assert.Fail(ShouldThrowExceptionMsg);
        }
        [TestMethod]
        public void GetMockDbSet_WithApproriateDbSet_ShouldReturnMockDbSet()
        {
            BD.BuildDbContext();
            BD.BuildDbSet<Product>();

            var mockDbSet = BD.GetMockDbSet<Product>();

            Assert.IsInstanceOfType(mockDbSet, typeof(Mock<DbSet<Product>>));
        }
        [TestMethod]
        [ExpectedException(typeof(NotFoundServiceMock))]
        public void GetServiceMock_WithoutService_ShouldThrowException()
        {
            BD.GetServiceMock<ICurrentDateTime>();

            Assert.Fail(ShouldThrowExceptionMsg);
        }
        [TestMethod]
        public void GetServiceMock_WithService_ShouldReturnService()
        {
            BD.BuildService<ICurrentDateTime>();

            var serviceMock = BD.GetServiceMock<ICurrentDateTime>();

            Assert.IsInstanceOfType(serviceMock, typeof(Mock<ICurrentDateTime>));
        }
        [TestMethod]
        public void BuildDbSetsInDbContext_WithoutArgs_ShouldMockDbSetInDbContext()
        {
            BD.BuildDbContext();
            var mockDbSet = BD.BuildDbSet<Product>();
            
            BD.BuildDbSetsInDbContext();

            //var products = MB.DbContextMock.Object.Products;
            //MB.DbContextMock.Verify();
            Assert.AreEqual(BD.DbContextMock.Object.Products, mockDbSet.Object);
        }
        [TestMethod]
        [ExpectedException(typeof(DbContextShouldBuildBeforeDbSets))]
        public void BuildDbSetsInDbContext_WithoutArgs_ShouldThrowDbContextException()
        {
            BD.BuildDbSetsInDbContext();

            Assert.Fail(ShouldThrowExceptionMsg);
        }
        [TestMethod]
        [ExpectedException(typeof(DbContextShouldContainAppropriateDbSet))]
        public void BuildDbSetsInDbContext_WithoutArgs_ShouldThrowDbSetException()
        {
            BD.BuildDbContext();
            BD.BuildDbSet<ICurrentDateTime>();

            BD.BuildDbSetsInDbContext();

            Assert.Fail(ShouldThrowExceptionMsg);
        }
        [TestMethod]
        public void BuildDbSetsInDbContext_WithoutArgs_DbSetBeforeCallintThisMethodContainsNull()
        {
            BD.BuildDbContext();
            BD.BuildDbSet<Product>();

            Assert.IsNull(BD.DbContextMock.Object.Products);
        }

    }
}
