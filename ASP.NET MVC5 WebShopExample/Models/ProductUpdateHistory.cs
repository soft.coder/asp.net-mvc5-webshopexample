﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class ProductUpdateHistory: Product
    {
        public DateTime UpdateDate { get; set; }
        public string UpdatedByUserId { get; set; }
        public virtual ApplicationUser UpdatedByUser { get; set; }
        public int CurrentProductId { get; set; }
        [ForeignKey("CurrentProductId")]
        public virtual Product CurrentProduct { get; set; }
    }
}