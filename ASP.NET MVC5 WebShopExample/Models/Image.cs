﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class Image
    {
        public int ImageId { get; set; }
        [Required]
        public string Name { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [ScaffoldColumn(false)]
        public int Width { get; set; }
        [ScaffoldColumn(false)]
        public int Height { get; set; }
        [DataType(DataType.Upload)]
        public byte[] Data { get; set; }
        [ScaffoldColumn(false)]
        public string MimeType { get; set; }
        public DateTime UploadedDate { get; set; }
        public string UploadedByUserId { get; set; }
        public virtual ApplicationUser UploadedByUser { get; set; }
        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public int? DiscountId { get; set; }
        public virtual Discount Discount { get; set; }
        public int? ReviewId { get; set; }
        public virtual Review Review { get; set; }
        public int? ReviewCommentId { get; set; }
        public virtual ReviewComment ReviewComment { get; set; }
        public int? QuestionId { get; set; }
        public virtual Question Question { get; set; }
        public int? AnswerId { get; set; }
        public virtual Answer Answer { get; set; }
    }
}