﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class ReviewComment
    {
        public int ReviewCommentId { get; set; }
        [Required, DataType(DataType.MultilineText)]
        public string Content { get; set; }
        [Required]
        public DateTime Created { get; set; }
        public DateTime? Updated { get; set; }
        public int? ReplyToId { get; set; }
        [ForeignKey("ReplyToId")]
        public virtual ReviewComment ReplyTo { get; set; }
        public int ReviewId { get; set; }
        public Review Review { get; set; }
        public string CreatedByUserId { get; set; }
        public virtual ApplicationUser CreatedByUser { get; set; }
        public virtual ICollection<Image> Images { get; set; }
    }
}