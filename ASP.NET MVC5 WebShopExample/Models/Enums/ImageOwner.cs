﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models.Enums
{
    public enum ImageOwner
    {
        Product,
        Category,
        Discount,
        Review,
        Question,
        Answer
    }
}