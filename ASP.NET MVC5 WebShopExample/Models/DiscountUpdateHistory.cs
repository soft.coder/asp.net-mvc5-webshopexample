﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class DiscountUpdateHistory: Discount
    {
        public DateTime UpdatedDate { get; set; }
        public string UpdatedByUserId { get; set; }
        public virtual ApplicationUser UpdatedByUser { get; set; }
        public int CurrentDiscountId { get; set; }
        [ForeignKey("CurrentDiscountId")]
        public virtual Discount CurrentDiscount { get; set; }
    }
}