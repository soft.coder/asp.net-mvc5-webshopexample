﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class SpecificationType
    {
        public int SpecificationTypeId { get; set; }
        [Required, Index, MaxLength(100)]
        public string Name { get; set; }
        [Required, Index, MaxLength(50)]
        public string Unit { get; set; }
        public string Filter { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Specification> Specifications { get; set; }
    }
}