﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class ProductInOrder
    {
        public int ProductInOrderId { get; set; }
        [Range(1, int.MaxValue)]
        public int Count { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        [Required]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        [Required]
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
    }
}