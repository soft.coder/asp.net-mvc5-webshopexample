﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class Discount
    {
        public int DiscountId { get; set; }
        [Required]
        public string Title { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Range(0, 100)]
        public int Percent { get; set; }
        [Index]
        public DateTime StartDate { get; set; }
        [Index]
        public DateTime EndDate { get; set; }
        [Timestamp, ScaffoldColumn(false)]
        public byte[] RowVersion { get; set; }
        [ScaffoldColumn(false)]
        public string CreatedByUserId { get; set; }
        public virtual ApplicationUser CreatedByUser { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Image> Images { get; set; }
    }
}