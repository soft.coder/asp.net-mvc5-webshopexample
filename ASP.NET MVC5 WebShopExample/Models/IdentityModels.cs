﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq.Expressions;
using System.Diagnostics;
using Microsoft.VisualBasic.Logging;
using System.IO;
using System.Web;
using System.Reflection;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<ReviewComment> ReviewComments { get; set; }
        public virtual ICollection<Review> HelpfullReviews { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Question> QuestionsWithPositiveVoice { get; set; }
        public virtual ICollection<Question> QuestionsWithNegativeVoice { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<Answer> HelpfulAnswers { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<CategoryUpdateHistory> CategoryUpdateHistories { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<ProductUpdateHistory> ProductUpdateHistories { get; set; }
        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<DiscountUpdateHistory> DiscountUpdateHistories { get; set; }
        public virtual ICollection<Image> Images { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public virtual DbSet<Answer> Answers { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CategoryUpdateHistory> CategoryUpdateHistories { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductInOrder> ProductInOrders { get; set; }
        public virtual DbSet<ProductUpdateHistory> ProductUpdateHistories { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Review> Reviews { get; set; }
        public virtual DbSet<ReviewComment> ReviewComments { get; set; }
        public virtual DbSet<Specification> Specifications { get; set; }
        public virtual DbSet<SpecificationType> SpecificationTypes { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            //FileLogTraceListener fileLogger = new FileLogTraceListener("ef6sql")
            //{
            //    Location = LogFileLocation.Custom,
            //    CustomLocation = Path.Combine(HttpRuntime.AppDomainAppPath, "Logs"),
            //    BaseFileName = "ef6-sql",
            //    LogFileCreationSchedule = LogFileCreationScheduleOption.Daily,

            //};
            //Trace.Listeners.Add(fileLogger);
            //Database.Log = msg => Trace.Write(msg);
        }

        public virtual DbEntityEntry<TEntity> GetEntry<TEntity>(TEntity entity) where TEntity: class
        {
            return base.Entry(entity);
        }
        [Obsolete("Use instead virtual 'GetEntry<TEntity>(TEntity entity, EntityState state)'")]
        public new void Entry<TEntity>(TEntity entity)
        {
            base.Entry(entity);
        }
        public virtual void ChangeEntryState<TEntity>(TEntity entity, EntityState state) where TEntity : class
        {
            base.Entry(entity).State = state;
        }
        public virtual Task CollectionLoadAsync<TEntity, TEntityCollection>(TEntity entity, Expression<Func<TEntity, ICollection<TEntityCollection>>> expression)
            where TEntity : class
            where TEntityCollection : class
        {
            return base.Entry(entity).Collection(expression).LoadAsync();
        }
        public virtual TEntity Attach<TEntity>(DbSet<TEntity> dbSet, TEntity entity) where TEntity: class
         
            => dbSet.Attach(entity);

        public virtual void SetModifiedPropertyState<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> propExpression) where TEntity: class
        {
            base.Entry(entity).Property(propExpression).IsModified = true;
        }
        public virtual void SetModifiedPropertyState<TEntity>(TEntity entity, IList<string> propNames) where TEntity : class
        {
            foreach (var propName in propNames)
            {
                base.Entry(entity).Property(propName).IsModified = true;
            }
        }

        public static ApplicationDbContext Create()
        {
            var c = "case";
            switch (c)
            {
                case "case":
                    break;
                case "case2":
                    break;
                default:
                    break;
            }
            return new ApplicationDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // many to many relationships
            modelBuilder.Entity<Category>()
                .HasMany(c => c.SpecificationTypes)
                .WithMany(st => st.Categories)
                .Map(m =>
                {
                    m.MapLeftKey("CategoryId");
                    m.MapRightKey("SpecificationTypeId");
                });
            modelBuilder.Entity<Question>()
                .HasMany(q => q.UsersWithNegativeVoice)
                .WithMany(u => u.QuestionsWithNegativeVoice)
                .Map(m =>
                {
                    m.ToTable("QuestionApplicationUsersWithNegativeVoice")
                        .MapLeftKey("QuestionId")
                        .MapRightKey("ApplicationUserId");

                });
            modelBuilder.Entity<Question>()
                .HasMany(q => q.UsersWithPositiveVoice)
                .WithMany(u => u.QuestionsWithPositiveVoice)
                .Map(m =>
                {
                    m.ToTable("QuestionApplicationUsersWithPositiveVoice")
                        .MapLeftKey("QuestionId")
                        .MapRightKey("ApplicationUserId");
                });
            modelBuilder.Entity<Answer>()
                .HasMany(a => a.HelpfulForUsers)
                .WithMany(u => u.HelpfulAnswers)
                .Map(m =>
                {
                    m.ToTable("AnswerHelpfulForApplicationUsers")
                        .MapLeftKey("AnswerId")
                        .MapRightKey("ApplicationUserId");

                });
            modelBuilder.Entity<Review>()
                .HasMany(r => r.HelpfulForUsers)
                .WithMany(u => u.HelpfullReviews)
                .Map(m =>
                {
                    m.ToTable("ReviewHelpfulForApplicationUsers")
                        .MapLeftKey("ReviewId")
                        .MapRightKey("HelpfulForApplicationUserId");
                });
            modelBuilder.Entity<Product>()
                .HasMany(p => p.Discounts)
                .WithMany(d => d.Products)
                .Map(m =>
                {
                    m.ToTable("ProductWithDiscount")
                        .MapLeftKey("ProductId")
                        .MapRightKey("DiscountId");
                });
            modelBuilder.Entity<Image>()
                .Property(i => i.Data)
                .IsRequired();
            modelBuilder.Entity<Image>()
                .Property(i => i.MimeType)
                .IsRequired();
        }
    }
}