﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class Answer
    {
        public int AnswerId { get; set; }
        [Required, DataType(DataType.MultilineText)]
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
        public string CreatedByUserId { get; set; }
        public virtual ApplicationUser CreatedByUser { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<ApplicationUser> HelpfulForUsers { get; set; }
        
    }
}