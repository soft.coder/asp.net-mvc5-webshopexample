﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        [Required, Index, MaxLength(200)]
        public string Name { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DataType(DataType.Currency), Column(TypeName = "money")]
        [Range(0.0001, double.MaxValue)]
        public decimal Cost { get; set; }
        [ScaffoldColumn(false)]
        public DateTime CreatedDate { get; set; }
        [ScaffoldColumn(false)]
        public DateTime? LastUpdateDate { get; set; }
        [Timestamp, ScaffoldColumn(false)]
        public byte[] RowVersion { get; set; }
        [ScaffoldColumn(false)]
        public string CreatedByUserId { get; set; }
        public virtual ApplicationUser CreatedByUser { get; set; }
        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Specification> Specifications { get; set; }
    }
}