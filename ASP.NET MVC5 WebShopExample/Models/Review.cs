﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class Review
    {
        public int ReviewId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required, DataType(DataType.MultilineText)]
        public string Advantage { get; set; }
        [Required, DataType(DataType.MultilineText)]
        public string Disadvantage { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public string CreatedByUserId { get; set; }
        public virtual ApplicationUser CreatedByUser { get; set; }
        public virtual ICollection<ApplicationUser> HelpfulForUsers { get; set; }
        public virtual ICollection<ReviewComment> Comments { get; set; }
        public virtual ICollection<Image> Images { get; set; }
    }
}