﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class Category
    {
        [Display(Name = "Category ID")]
        public int CategoryId { get; set; }
        [Required, Index, MaxLength(200)]
        public string Name { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public int? ParentCategoryId { get; set; }
        [ForeignKey("ParentCategoryId"), Display(Name = "Parent Category")]
        public virtual Category ParentCategory { get; set; }
        [ScaffoldColumn(false)]
        public string CreatedByUserId { get; set; }
        [Display(Name = "Created by user")]
        public virtual ApplicationUser CreatedByUser { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<SpecificationType> SpecificationTypes { get; set; }
        public virtual ICollection<Product> Products { get; set; }

        public bool IsChanged(Category newCategory)
        {
            if (Name != newCategory.Name)
                return true;
            if (ParentCategoryId != newCategory.ParentCategoryId)
                return true;
            return false;
        }
    }
}