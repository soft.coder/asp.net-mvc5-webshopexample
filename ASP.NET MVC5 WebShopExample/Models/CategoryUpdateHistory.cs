﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class CategoryUpdateHistory: Category
    {
        public CategoryUpdateHistory()
        { }
        public CategoryUpdateHistory(Category oldCategory, Category newCategory, string updatedByUserId, DateTime updateDate)
        {
            if (oldCategory.Name != newCategory.Name)
                Name = newCategory.Name;
            if (oldCategory.ParentCategoryId != newCategory.ParentCategoryId)
                ParentCategoryId = newCategory.ParentCategoryId;
            UpdateDate = updateDate;
            CurrentCategoryId = newCategory.CategoryId;
            RowVersion = newCategory.RowVersion;
            UpdatedByUserId = updatedByUserId;
        }
        
        public DateTime UpdateDate { get; set; }
        public string UpdatedByUserId { get; set; }
        public virtual ApplicationUser UpdatedByUser { get; set; }
        public int CurrentCategoryId { get; set; }
        [ForeignKey("CurrentCategoryId")]
        public virtual Category CurrentCategory { get; set; }
    }
}