﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class Specification
    {
        public int SpecificationId { get; set; }
        [Index, MaxLength(100)]
        public string Value { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int SpecificationTypeId { get; set; }
        public virtual SpecificationType SpecificationType { get; set; }
    }
}