﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class ProductCreateImagesViewModel
    {
        public IEnumerable<ImageLoadViewModel> Images { get; set; }
        public Product Product { get; set; }
    }
}