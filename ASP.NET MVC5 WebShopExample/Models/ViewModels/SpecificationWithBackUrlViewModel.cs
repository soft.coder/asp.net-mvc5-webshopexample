﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class SpecificationWithBackUrlViewModel: ViewModel
    {
        public Specification Specification { get; set; }
        public string BackUrl { get; set; }
    }
}