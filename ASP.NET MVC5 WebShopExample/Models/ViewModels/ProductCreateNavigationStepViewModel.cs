﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class ProductCreateNavigationStepViewModel
    {
        public ProductCreateNavigationStep NavigationStep { get; set; }
    }
    public enum ProductCreateNavigationStep
    {
        Main,
        CreateSpecifications,
        UploadImages
    }

}