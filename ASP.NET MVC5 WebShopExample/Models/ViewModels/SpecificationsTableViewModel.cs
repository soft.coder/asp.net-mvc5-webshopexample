﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class SpecificationsTableViewModel: ViewModel
    {
        public Product Product { get; set; }
        public bool IsShowFooter { get; set; }
        public bool IsShowCaption { get; set; }
        public bool IsShowActionButtons { get; set; }
    }
}