﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Profile;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class ProductEditViewModel: ViewModel
    {
        public Product Product { get; set; }
        public SelectList SelectListOfCategories { get; set; }
        public IEnumerable<ImageLoadViewModel> Images { get; set; }
    }
}