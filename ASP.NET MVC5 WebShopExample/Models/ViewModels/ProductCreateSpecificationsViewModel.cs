﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class ProductCreateSpecificationsViewModel
    {
        public Product Product { get; set; }
        public SelectList SelectListSpecificationTypes { get; set; }
    }
}