﻿using ASP.NET_MVC5_WebShopExample.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class ImageEditViewModel: ImageViewModel
    {
        public int OwnerId { get; set; }
        public string ImageServerError { get; set; }
    }
}