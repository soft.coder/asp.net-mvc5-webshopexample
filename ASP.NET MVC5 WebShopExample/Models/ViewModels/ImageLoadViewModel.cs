﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class ImageLoadViewModel: ViewModel
    {
        public int ImageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}