﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class ImageListViewModel: ViewModel
    {
        public IEnumerable<ImageLoadViewModel> Images { get; set; }
        public string BackUrl { get; set; }
        public string Controller { get; set; }
        public int? OwnerId { get; set; }
    }
}