﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class SpecificationIndexViewModel: ViewModel
    {
        public Product Product { get; set; }
        public IEnumerable<Specification> Specifications { get; set; }
    }
}