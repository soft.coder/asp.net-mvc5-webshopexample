﻿using ASP.NET_MVC5_WebShopExample.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models.ViewModels
{
    public class ImageViewModel: ViewModel
    {
        public Image Image { get; set; }
        public string BackUrl { get; set; }
    }
}