﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Models
{
    public class Question
    {
        public int QuestionId { get; set; }
        [Required, DataType(DataType.MultilineText)]
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public string CreatedByUserId { get; set; }
        public virtual ApplicationUser CreatedByUser { get; set; }
        public virtual ICollection<ApplicationUser> UsersWithNegativeVoice { get; set; }
        public virtual ICollection<ApplicationUser> UsersWithPositiveVoice { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<Image> Images { get; set; }
    }
}