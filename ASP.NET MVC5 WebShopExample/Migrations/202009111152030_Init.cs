﻿namespace ASP.NET_MVC5_WebShopExample.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        AnswerId = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(),
                        QuestionId = c.Int(nullable: false),
                        CreatedByUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AnswerId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedByUserId)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .Index(t => t.QuestionId)
                .Index(t => t.CreatedByUserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ParentCategoryId = c.Int(),
                        CreatedByUserId = c.String(maxLength: 128),
                        UpdateDate = c.DateTime(),
                        UpdatedByUserId = c.String(maxLength: 128),
                        CurrentCategoryId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CategoryId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedByUserId)
                .ForeignKey("dbo.Categories", t => t.ParentCategoryId)
                .ForeignKey("dbo.Categories", t => t.CurrentCategoryId)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedByUserId)
                .Index(t => t.Name)
                .Index(t => t.ParentCategoryId)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.UpdatedByUserId)
                .Index(t => t.CurrentCategoryId);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ImageId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        Data = c.Binary(nullable: false),
                        MimeType = c.String(nullable: false),
                        UploadedDate = c.DateTime(nullable: false),
                        UploadedByUserId = c.String(maxLength: 128),
                        ProductId = c.Int(),
                        CategoryId = c.Int(),
                        DiscountId = c.Int(),
                        ReviewId = c.Int(),
                        ReviewCommentId = c.Int(),
                        QuestionId = c.Int(),
                        AnswerId = c.Int(),
                    })
                .PrimaryKey(t => t.ImageId)
                .ForeignKey("dbo.Answers", t => t.AnswerId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .ForeignKey("dbo.Discounts", t => t.DiscountId)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .ForeignKey("dbo.Questions", t => t.QuestionId)
                .ForeignKey("dbo.ReviewComments", t => t.ReviewCommentId)
                .ForeignKey("dbo.Reviews", t => t.ReviewId)
                .ForeignKey("dbo.AspNetUsers", t => t.UploadedByUserId)
                .Index(t => t.UploadedByUserId)
                .Index(t => t.ProductId)
                .Index(t => t.CategoryId)
                .Index(t => t.DiscountId)
                .Index(t => t.ReviewId)
                .Index(t => t.ReviewCommentId)
                .Index(t => t.QuestionId)
                .Index(t => t.AnswerId);
            
            CreateTable(
                "dbo.Discounts",
                c => new
                    {
                        DiscountId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                        Percent = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CreatedByUserId = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedByUserId = c.String(maxLength: 128),
                        CurrentDiscountId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.DiscountId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedByUserId)
                .ForeignKey("dbo.Discounts", t => t.CurrentDiscountId)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedByUserId)
                .Index(t => t.StartDate)
                .Index(t => t.EndDate)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.UpdatedByUserId)
                .Index(t => t.CurrentDiscountId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Description = c.String(),
                        Cost = c.Decimal(nullable: false, storeType: "money"),
                        CreatedDate = c.DateTime(nullable: false),
                        LastUpdateDate = c.DateTime(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CreatedByUserId = c.String(maxLength: 128),
                        CategoryId = c.Int(),
                        UpdateDate = c.DateTime(),
                        UpdatedByUserId = c.String(maxLength: 128),
                        CurrentProductId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedByUserId)
                .ForeignKey("dbo.Products", t => t.CurrentProductId)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedByUserId)
                .Index(t => t.Name)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.CategoryId)
                .Index(t => t.UpdatedByUserId)
                .Index(t => t.CurrentProductId);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        QuestionId = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(),
                        ProductId = c.Int(nullable: false),
                        CreatedByUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.QuestionId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedByUserId)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.CreatedByUserId);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        ReviewId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Advantage = c.String(nullable: false),
                        Disadvantage = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(),
                        ProductId = c.Int(nullable: false),
                        CreatedByUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ReviewId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedByUserId)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.CreatedByUserId);
            
            CreateTable(
                "dbo.ReviewComments",
                c => new
                    {
                        ReviewCommentId = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(),
                        ReplyToId = c.Int(),
                        ReviewId = c.Int(nullable: false),
                        CreatedByUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ReviewCommentId)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedByUserId)
                .ForeignKey("dbo.ReviewComments", t => t.ReplyToId)
                .ForeignKey("dbo.Reviews", t => t.ReviewId, cascadeDelete: true)
                .Index(t => t.ReplyToId)
                .Index(t => t.ReviewId)
                .Index(t => t.CreatedByUserId);
            
            CreateTable(
                "dbo.Specifications",
                c => new
                    {
                        SpecificationId = c.Int(nullable: false, identity: true),
                        Value = c.String(maxLength: 100),
                        ProductId = c.Int(nullable: false),
                        SpecificationTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SpecificationId)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.SpecificationTypes", t => t.SpecificationTypeId, cascadeDelete: true)
                .Index(t => t.Value)
                .Index(t => t.ProductId)
                .Index(t => t.SpecificationTypeId);
            
            CreateTable(
                "dbo.SpecificationTypes",
                c => new
                    {
                        SpecificationTypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        Unit = c.String(nullable: false, maxLength: 50),
                        Filter = c.String(),
                    })
                .PrimaryKey(t => t.SpecificationTypeId)
                .Index(t => t.Name)
                .Index(t => t.Unit);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Int(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        LastUpdateDate = c.DateTime(),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ProductInOrders",
                c => new
                    {
                        ProductInOrderId = c.Int(nullable: false, identity: true),
                        Count = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        ProductId = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductInOrderId)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.ProductWithDiscount",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        DiscountId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductId, t.DiscountId })
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Discounts", t => t.DiscountId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.DiscountId);
            
            CreateTable(
                "dbo.QuestionApplicationUsersWithNegativeVoice",
                c => new
                    {
                        QuestionId = c.Int(nullable: false),
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.QuestionId, t.ApplicationUserId })
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId, cascadeDelete: true)
                .Index(t => t.QuestionId)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.QuestionApplicationUsersWithPositiveVoice",
                c => new
                    {
                        QuestionId = c.Int(nullable: false),
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.QuestionId, t.ApplicationUserId })
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId, cascadeDelete: true)
                .Index(t => t.QuestionId)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.ReviewHelpfulForApplicationUsers",
                c => new
                    {
                        ReviewId = c.Int(nullable: false),
                        HelpfulForApplicationUserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.ReviewId, t.HelpfulForApplicationUserId })
                .ForeignKey("dbo.Reviews", t => t.ReviewId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.HelpfulForApplicationUserId, cascadeDelete: true)
                .Index(t => t.ReviewId)
                .Index(t => t.HelpfulForApplicationUserId);
            
            CreateTable(
                "dbo.CategorySpecificationTypes",
                c => new
                    {
                        CategoryId = c.Int(nullable: false),
                        SpecificationTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CategoryId, t.SpecificationTypeId })
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.SpecificationTypes", t => t.SpecificationTypeId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.SpecificationTypeId);
            
            CreateTable(
                "dbo.AnswerHelpfulForApplicationUsers",
                c => new
                    {
                        AnswerId = c.Int(nullable: false),
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.AnswerId, t.ApplicationUserId })
                .ForeignKey("dbo.Answers", t => t.AnswerId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId, cascadeDelete: true)
                .Index(t => t.AnswerId)
                .Index(t => t.ApplicationUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AnswerHelpfulForApplicationUsers", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AnswerHelpfulForApplicationUsers", "AnswerId", "dbo.Answers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Orders", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ProductInOrders", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductInOrders", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Categories", "UpdatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Categories", "CurrentCategoryId", "dbo.Categories");
            DropForeignKey("dbo.CategorySpecificationTypes", "SpecificationTypeId", "dbo.SpecificationTypes");
            DropForeignKey("dbo.CategorySpecificationTypes", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Categories", "ParentCategoryId", "dbo.Categories");
            DropForeignKey("dbo.Images", "UploadedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Discounts", "UpdatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Discounts", "CurrentDiscountId", "dbo.Discounts");
            DropForeignKey("dbo.Products", "UpdatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Products", "CurrentProductId", "dbo.Products");
            DropForeignKey("dbo.Specifications", "SpecificationTypeId", "dbo.SpecificationTypes");
            DropForeignKey("dbo.Specifications", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Reviews", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Images", "ReviewId", "dbo.Reviews");
            DropForeignKey("dbo.ReviewHelpfulForApplicationUsers", "HelpfulForApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ReviewHelpfulForApplicationUsers", "ReviewId", "dbo.Reviews");
            DropForeignKey("dbo.Reviews", "CreatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ReviewComments", "ReviewId", "dbo.Reviews");
            DropForeignKey("dbo.ReviewComments", "ReplyToId", "dbo.ReviewComments");
            DropForeignKey("dbo.Images", "ReviewCommentId", "dbo.ReviewComments");
            DropForeignKey("dbo.ReviewComments", "CreatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.QuestionApplicationUsersWithPositiveVoice", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.QuestionApplicationUsersWithPositiveVoice", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.QuestionApplicationUsersWithNegativeVoice", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.QuestionApplicationUsersWithNegativeVoice", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Questions", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Images", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Questions", "CreatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Answers", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Images", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductWithDiscount", "DiscountId", "dbo.Discounts");
            DropForeignKey("dbo.ProductWithDiscount", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "CreatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Products", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Images", "DiscountId", "dbo.Discounts");
            DropForeignKey("dbo.Discounts", "CreatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Images", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Images", "AnswerId", "dbo.Answers");
            DropForeignKey("dbo.Categories", "CreatedByUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Answers", "CreatedByUserId", "dbo.AspNetUsers");
            DropIndex("dbo.AnswerHelpfulForApplicationUsers", new[] { "ApplicationUserId" });
            DropIndex("dbo.AnswerHelpfulForApplicationUsers", new[] { "AnswerId" });
            DropIndex("dbo.CategorySpecificationTypes", new[] { "SpecificationTypeId" });
            DropIndex("dbo.CategorySpecificationTypes", new[] { "CategoryId" });
            DropIndex("dbo.ReviewHelpfulForApplicationUsers", new[] { "HelpfulForApplicationUserId" });
            DropIndex("dbo.ReviewHelpfulForApplicationUsers", new[] { "ReviewId" });
            DropIndex("dbo.QuestionApplicationUsersWithPositiveVoice", new[] { "ApplicationUserId" });
            DropIndex("dbo.QuestionApplicationUsersWithPositiveVoice", new[] { "QuestionId" });
            DropIndex("dbo.QuestionApplicationUsersWithNegativeVoice", new[] { "ApplicationUserId" });
            DropIndex("dbo.QuestionApplicationUsersWithNegativeVoice", new[] { "QuestionId" });
            DropIndex("dbo.ProductWithDiscount", new[] { "DiscountId" });
            DropIndex("dbo.ProductWithDiscount", new[] { "ProductId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.ProductInOrders", new[] { "OrderId" });
            DropIndex("dbo.ProductInOrders", new[] { "ProductId" });
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.SpecificationTypes", new[] { "Unit" });
            DropIndex("dbo.SpecificationTypes", new[] { "Name" });
            DropIndex("dbo.Specifications", new[] { "SpecificationTypeId" });
            DropIndex("dbo.Specifications", new[] { "ProductId" });
            DropIndex("dbo.Specifications", new[] { "Value" });
            DropIndex("dbo.ReviewComments", new[] { "CreatedByUserId" });
            DropIndex("dbo.ReviewComments", new[] { "ReviewId" });
            DropIndex("dbo.ReviewComments", new[] { "ReplyToId" });
            DropIndex("dbo.Reviews", new[] { "CreatedByUserId" });
            DropIndex("dbo.Reviews", new[] { "ProductId" });
            DropIndex("dbo.Questions", new[] { "CreatedByUserId" });
            DropIndex("dbo.Questions", new[] { "ProductId" });
            DropIndex("dbo.Products", new[] { "CurrentProductId" });
            DropIndex("dbo.Products", new[] { "UpdatedByUserId" });
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropIndex("dbo.Products", new[] { "CreatedByUserId" });
            DropIndex("dbo.Products", new[] { "Name" });
            DropIndex("dbo.Discounts", new[] { "CurrentDiscountId" });
            DropIndex("dbo.Discounts", new[] { "UpdatedByUserId" });
            DropIndex("dbo.Discounts", new[] { "CreatedByUserId" });
            DropIndex("dbo.Discounts", new[] { "EndDate" });
            DropIndex("dbo.Discounts", new[] { "StartDate" });
            DropIndex("dbo.Images", new[] { "AnswerId" });
            DropIndex("dbo.Images", new[] { "QuestionId" });
            DropIndex("dbo.Images", new[] { "ReviewCommentId" });
            DropIndex("dbo.Images", new[] { "ReviewId" });
            DropIndex("dbo.Images", new[] { "DiscountId" });
            DropIndex("dbo.Images", new[] { "CategoryId" });
            DropIndex("dbo.Images", new[] { "ProductId" });
            DropIndex("dbo.Images", new[] { "UploadedByUserId" });
            DropIndex("dbo.Categories", new[] { "CurrentCategoryId" });
            DropIndex("dbo.Categories", new[] { "UpdatedByUserId" });
            DropIndex("dbo.Categories", new[] { "CreatedByUserId" });
            DropIndex("dbo.Categories", new[] { "ParentCategoryId" });
            DropIndex("dbo.Categories", new[] { "Name" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Answers", new[] { "CreatedByUserId" });
            DropIndex("dbo.Answers", new[] { "QuestionId" });
            DropTable("dbo.AnswerHelpfulForApplicationUsers");
            DropTable("dbo.CategorySpecificationTypes");
            DropTable("dbo.ReviewHelpfulForApplicationUsers");
            DropTable("dbo.QuestionApplicationUsersWithPositiveVoice");
            DropTable("dbo.QuestionApplicationUsersWithNegativeVoice");
            DropTable("dbo.ProductWithDiscount");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.ProductInOrders");
            DropTable("dbo.Orders");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.SpecificationTypes");
            DropTable("dbo.Specifications");
            DropTable("dbo.ReviewComments");
            DropTable("dbo.Reviews");
            DropTable("dbo.Questions");
            DropTable("dbo.Products");
            DropTable("dbo.Discounts");
            DropTable("dbo.Images");
            DropTable("dbo.Categories");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Answers");
        }
    }
}
