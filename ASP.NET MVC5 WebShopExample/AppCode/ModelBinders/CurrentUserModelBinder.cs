﻿using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.AppCode.ModelBinders
{
    public class CurrentUserModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return new CurrentUser(controllerContext.HttpContext.User);
        }
    }
}