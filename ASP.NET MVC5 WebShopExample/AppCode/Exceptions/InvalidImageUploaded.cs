﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.AppCode.Exceptions
{
    public class InvalidImageUploaded : Exception
    {
        public InvalidImageUploaded(string message) : base(message)
        {
        }

        public InvalidImageUploaded(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}