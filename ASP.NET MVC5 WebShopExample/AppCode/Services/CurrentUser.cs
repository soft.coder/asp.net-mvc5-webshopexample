﻿using ASP.NET_MVC5_WebShopExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.AppCode.Services
{
    public class CurrentUser: ICurrentUser
    {
        public CurrentUser(IPrincipal userPrincipal)
        {
            UserPrincipal = userPrincipal;
        }

        public IPrincipal UserPrincipal { get; }

        public string GetUserId()
        {
            using (var db = new ApplicationDbContext())
            {
                return (from u in db.Users
                        where u.Email == UserPrincipal.Identity.Name
                        select new { ID = u.Id }).Single().ID;
            }
        }

    }
}