﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.AppCode.Services
{
    public class CurrentDateTime : ICurrentDateTime
    {
        public DateTime Get()
        {
            return DateTime.Now;
        }
    }
}