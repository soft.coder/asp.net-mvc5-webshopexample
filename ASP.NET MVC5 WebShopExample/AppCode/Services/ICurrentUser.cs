﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP.NET_MVC5_WebShopExample.AppCode.Services
{
    public interface ICurrentUser
    {
        string GetUserId();
    }
}
