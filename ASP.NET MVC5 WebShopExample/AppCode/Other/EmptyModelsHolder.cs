﻿using ASP.NET_MVC5_WebShopExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.AppCode.Other
{
    public class EmptyModelsHolder
    {
        public Product Product { get; } = new Product();
        public Category Category { get; } = new Category();
        public Specification Specification { get; } = new Specification();
        public SpecificationType SpecificationType { get; } = new SpecificationType();
        public Image Image { get; } = new Image();
        public Question Question { get; } = new Question();
        public Answer Answer { get; } = new Answer();
        public Review Review { get; } = new Review();
        public ReviewComment ReviewComment { get; } = new ReviewComment();
        public Discount Discount { get; } = new Discount();

    }
}