﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.AppCode.ViewEngines
{
    public class ImageOwnerViewEngine: RazorViewEngine
    {
        private readonly string[] NewLocations = new string[]
        {
            "~/Views/Images/{0}.cshtml"
        };
        public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
        {
            var controllerName = GetControllerName(controllerContext);
            if (controllerName.EndsWith("Images"))
            {
                ViewLocationFormats = NewLocations.Union(ViewLocationFormats).ToArray();
                return base.FindView(controllerContext, viewName, masterName, useCache);
            }
            else
                return base.FindView(controllerContext, viewName, masterName, useCache);
        }
        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            var controllerName = GetControllerName(controllerContext);
            if (controllerName.EndsWith("Images"))
            {
                PartialViewLocationFormats = NewLocations.Union(PartialViewLocationFormats).ToArray();
                return base.FindPartialView(controllerContext, partialViewName, useCache);
            }
            else
                return base.FindPartialView(controllerContext, partialViewName, useCache);
        }
        private string GetControllerName(ControllerContext controllerContext)

            => controllerContext.RouteData.Values["controller"] as string;
    }
}