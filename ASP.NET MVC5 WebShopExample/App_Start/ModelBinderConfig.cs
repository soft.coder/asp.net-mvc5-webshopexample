﻿using ASP.NET_MVC5_WebShopExample.AppCode.ModelBinders;
using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.App_Start
{
    public class ModelBinderConfig
    {
        internal static void RegisterModelBinders(System.Web.Mvc.ModelBinderDictionary binders)
        {
            binders.Add(typeof(ICurrentUser), new CurrentUserModelBinder());
        }
    }
}