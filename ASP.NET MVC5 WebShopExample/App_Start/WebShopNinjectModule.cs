﻿//using Ninject.Modules;
using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using ASP.NET_MVC5_WebShopExample.Models;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.App_Start
{
    public class WebShopNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ApplicationDbContext>().ToSelf();
            Kernel.Bind<ICurrentDateTime>().To<CurrentDateTime>();
        }
    }
}