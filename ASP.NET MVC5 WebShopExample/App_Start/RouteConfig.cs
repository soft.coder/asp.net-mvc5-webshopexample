﻿using ASP.NET_MVC5_WebShopExample.App_Start;
using ASP.NET_MVC5_WebShopExample.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ASP.NET_MVC5_WebShopExample
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: RouteNames.ImagesWithId,
                url: "{controller}/{action}/owner-id/{ownerId}/id/{id}",
                defaults: null,
                constraints: new { action = "Edit|Details|Delete"}
                );

            routes.MapRoute(
                name: RouteNames.ImagesWithoutId,
                url: "{controller}/{action}/owner-id/{ownerId}",
                defaults: null,
                constraints: new { action = "Upload",}
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: RouteNames.Specifications,
                url: "Products/{productId}/Specifications/{action}/{id}",
                defaults: new { controller = "Specifications", id = UrlParameter.Optional },
                constraints: new { productId = "\\d+"}
                );

        }
    }
}
