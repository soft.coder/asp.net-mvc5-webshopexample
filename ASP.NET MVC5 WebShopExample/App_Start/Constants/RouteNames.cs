﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_MVC5_WebShopExample.Constants
{
    public class RouteNames
    {
        static public string Specifications { get; } = nameof(Specifications);
        static public string ImagesWithoutId { get; } = nameof(ImagesWithoutId);
        static public string ImagesWithId { get; } = nameof(ImagesWithId);
    }
}