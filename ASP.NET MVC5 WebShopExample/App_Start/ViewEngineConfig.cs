﻿using ASP.NET_MVC5_WebShopExample.AppCode.ViewEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.App_Start
{
    public class ViewEngineConfig
    {
        public static void RegisterViewEngines(ViewEngineCollection viewEngines)
        {
            viewEngines.Add(new ImageOwnerViewEngine());
        }
    }
}