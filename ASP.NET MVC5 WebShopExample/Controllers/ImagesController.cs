﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASP.NET_MVC5_WebShopExample.Models;
using ASP.NET_MVC5_WebShopExample.Models.Enums;
using ASP.NET_MVC5_WebShopExample.Models.ViewModels;
using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using ASP.NET_MVC5_WebShopExample.AppCode.Other;

using SDImage = System.Drawing.Image;
using System.Drawing.Imaging;
using Ninject.Infrastructure.Language;
using ASP.NET_MVC5_WebShopExample.AppCode.Exceptions;
using System.IO;

namespace ASP.NET_MVC5_WebShopExample.Controllers
{
    public abstract class ImagesController : BaseController
    {
        protected ApplicationDbContext db = new ApplicationDbContext();

        public EmptyModelsHolder Empty { get; } = new EmptyModelsHolder();

        public ICurrentDateTime CurrentDateTime { get; }

        public ImagesController(ICurrentDateTime currentDateTime) : base()
        {
            CurrentDateTime = currentDateTime;
        }

        // GET: Images
        [HttpGet]
        public FileContentResult GetImage(int id)
        {
            Image image = db.Images.Find(id);
            if (image != null)
                return File(image.Data, image.MimeType);
            else
                return null;
        }

        // GET: Images/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Image image = await db.Images.FindAsync(id);
            if (image == null)
            {
                return HttpNotFound();
            }
            return View(image);
        }

        // GET: Images/Upload
        public ActionResult Upload(int ownerId, string backUrl)
        {
            var viewModel = new ImageEditViewModel()
            {
                Image = new Image(),
                BackUrl = backUrl,
                OwnerId = ownerId,
            };
            return View(viewModel);
        }

        // POST: Images/Upload
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Upload(ICurrentUser currentUser,
            [Bind(Include = "Name,Description")] Image image,
            int ownerId, string backUrl,
            HttpPostedFileBase postedFile)
        {
            image.UploadedByUserId = currentUser.GetUserId();
            image.UploadedDate = CurrentDateTime.Get();
            SetImageOwner(image, ownerId);
            string serverError = string.Empty;
            try
            {
                CheckImageAndGetSize(postedFile, out int imageWidth, out int imageHeight);
                image.MimeType = postedFile.ContentType;
                image.Data = new byte[postedFile.ContentLength];
                await postedFile.InputStream.ReadAsync(image.Data, 0, postedFile.ContentLength);
                image.Width = imageWidth;
                image.Height = imageHeight;
                if (ModelState.IsValid)
                {
                    db.Images.Add(image);
                    await db.SaveChangesAsync();
                    if (backUrl != null)
                        return Redirect(backUrl);
                    else
                        return RedirectToAction("Index");
                }
            }
            catch (InvalidImageUploaded ex)
            {
                serverError = ex.Message;
            }
            var viewModel = new ImageEditViewModel()
            {
                Image = image,
                BackUrl = backUrl,
                OwnerId = ownerId,
                ImageServerError = serverError,
            };

            return View(viewModel);
        }

       

        // GET: Images/Edit/5
        public async Task<ActionResult> Edit(int ownerId, int id, string backUrl)
        {
            try
            {
                Image image = GetOwnerImage(ownerId, id);
                return View(new ImageEditViewModel()
                {
                    Image = image,
                    OwnerId = ownerId,
                    BackUrl = backUrl,
                });
            }
            catch(InvalidOperationException ex)
            {
                return HttpNotFound();
            }
        }

        // POST: Images/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ICurrentUser currentUser,
            [Bind(Include = "ImageId, Name,Description")] Image image, 
            int ownerId, string backUrl, HttpPostedFileBase postedFile)
        {
            image.UploadedByUserId = currentUser.GetUserId();
            image.UploadedDate = CurrentDateTime.Get();
            
            var modifiedProperties = new List<string>() 
            { 
                nameof(image.UploadedByUserId), 
                nameof(image.UploadedDate),
                nameof(image.Name),
                nameof(image.Description)
            };
            string serverError = string.Empty;
            try
            {
                if (postedFile != null)
                {
                    CheckImageAndGetSize(postedFile, out int imageWidth, out int imageHeight);
                    image.MimeType = postedFile.ContentType;
                    image.Data = new byte[postedFile.ContentLength];
                    await postedFile.InputStream.ReadAsync(image.Data, 0, postedFile.ContentLength);
                    image.Width = imageWidth;
                    image.Height = imageHeight;
                    modifiedProperties.AddRange(new List<string>()
                    {
                        nameof(image.MimeType),
                        nameof(image.Data),
                        nameof(image.Width),
                        nameof(image.Height)
                    });
                }
                if (ModelState.IsValid)
                {
                    db.Attach(db.Images, image);
                    
                    db.SetModifiedPropertyState(image, modifiedProperties);
                    try
                    {
                        db.Configuration.ValidateOnSaveEnabled = false;
                        await db.SaveChangesAsync();
                    }
                    finally
                    {
                        db.Configuration.ValidateOnSaveEnabled = true;
                    }
                    if (backUrl != null)
                        return Redirect(backUrl);
                    else
                        return RedirectToAction("Index");
                }
            }
            catch (InvalidImageUploaded ex)
            {
                serverError = ex.Message;
            }
            var viewModel = new ImageEditViewModel()
            {
                Image = image,
                BackUrl = backUrl,
                OwnerId = ownerId,
                ImageServerError = serverError,
            };
            return View(viewModel);
        }

        // GET: Images/Delete/5
        public async Task<ActionResult> Delete(int? id, string backUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Image image = await db.Images.FindAsync(id);
            if (image == null)
            {
                return HttpNotFound();
            }
            var viewModel = new ImageViewModel()
            {
                Image = image,
                BackUrl = backUrl
            };
            return View(viewModel);
        }

        // POST: Images/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, string backUrl)
        {
            Image image = await db.Images.FindAsync(id);
            db.Images.Remove(image);
            await db.SaveChangesAsync();
            return Redirect(backUrl);
        }

        protected abstract Task<List<ImageLoadViewModel>> GetLoadInfoOfOwnerImages(int ownerId);

        protected abstract Image GetOwnerImage(int ownerId, int id);

        protected abstract void SetImageOwner(Image image, int ownerId);


        private void CheckImageAndGetSize(HttpPostedFileBase postedFile, out int width, out int height)
        {
            //width = height = null;
            if (postedFile == null)
                throw new InvalidImageUploaded("Choose file for upload");
            var ValidFormats = new Dictionary<string, ImageFormat>()
            {
                ["image/png"] = ImageFormat.Png,
                ["image/jpeg"] = ImageFormat.Jpeg,
                ["image/gif"] = ImageFormat.Gif
            };
            var fileExtensions = ValidFormats.Keys.Select(mimeType => mimeType.Replace("image/", ""));
            var strFiles = string.Join(", ", fileExtensions);
            string invalidFileType = $"Invalid type of file, supported only: {strFiles}";

            if (postedFile.ContentType.Contains("image") == false)
                throw new InvalidImageUploaded(invalidFileType);
            try
            {
                var validFormat = ValidFormats.Single(vf => vf.Key == postedFile.ContentType);
                var sdImage = SDImage.FromStream(postedFile.InputStream);
                if (validFormat.Value.Equals(sdImage.RawFormat) == false)
                    throw new InvalidImageUploaded(invalidFileType);

                width = sdImage.Width;
                height = sdImage.Height;
                postedFile.InputStream.Position = 0;
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidImageUploaded(invalidFileType, ex);
            }
            catch (ArgumentException ex)
            {
                throw new InvalidImageUploaded(invalidFileType, ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
