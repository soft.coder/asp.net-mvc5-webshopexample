﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASP.NET_MVC5_WebShopExample.Models;
using ASP.NET_MVC5_WebShopExample.AppCode.Services;

namespace ASP.NET_MVC5_WebShopExample.Controllers
{
    [Authorize]
    public class CategoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public CategoriesController(ApplicationDbContext dbContext)
        {
            db = dbContext;
        }

        // GET: Categories
        public async Task<ActionResult> Index()
        {
            var c = new Category();
            var categories = db.Categories.Include(nameof(c.CreatedByUser)).Include(nameof(c.ParentCategory));
            return View(await categories.ToListAsync());
        }

        // GET: Categories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            ViewBag.ParentCategories = new SelectList(db.Categories, "CategoryId", "Name");
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ICurrentUser currentUser,[Bind(Include = "Name,RowVersion,ParentCategoryId")] Category category)
        {
            category.CreatedByUserId = currentUser.GetUserId();
            if (ModelState.IsValid)
            {
                db.Categories.Add(category);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ParentCategories = new SelectList(db.Categories, "CategoryId", "Name", category.ParentCategoryId);
            return View(category);
        }

        // GET: Categories/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            
            var parentCategories = from c in db.Categories
                                   where c.CategoryId != category.CategoryId
                                   select new { Name = "ID" + c.CategoryId + ": " + c.Name, c.CategoryId };

            ViewBag.ParentCategories = new SelectList(parentCategories, "CategoryId", "Name", category.ParentCategoryId);
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ICurrentDateTime currentDate, ICurrentUser currentUser, [Bind(Include = "CategoryId,Name,RowVersion,ParentCategoryId")] Category category)
        {
            var oldCategory = await db.Categories.FindAsync(category.CategoryId);
            if (ModelState.IsValid && oldCategory.IsChanged(category))
            {
                string currentUserId = currentUser.GetUserId();
                var updateDate = currentDate.Get();
                // TODO: Replace new operation to factory or model binding for decreasing binding
                var categoryUpdate = new CategoryUpdateHistory(oldCategory, category, currentUserId, updateDate);
                db.CategoryUpdateHistories.Add(categoryUpdate);
                db.ChangeEntryState(category, EntityState.Modified);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            var parentCategories = from c in db.Categories
                                   where c.CategoryId != category.CategoryId
                                   select new { Name = "ID" + c.CategoryId + ": " + c.Name, c.CategoryId };

            ViewBag.ParentCategories = new SelectList(parentCategories, "CategoryId", "Name", category.ParentCategoryId);
            return View(category);
        }

        // GET: Categories/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Category category = await db.Categories.FindAsync(id);
            if (category != null)
            {
                db.Categories.Remove(category);
                await db.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
