﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASP.NET_MVC5_WebShopExample.Models;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core;
using ASP.NET_MVC5_WebShopExample.Models.ViewModels;
using ASP.NET_MVC5_WebShopExample.Constants;

namespace ASP.NET_MVC5_WebShopExample.Controllers
{
    public class SpecificationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private SpecificationType st { get; } = new SpecificationType();
        private Specification s { get; } = new Specification();
        private Product p { get; } = new Product();
        private Category c { get; } = new Category();

        // GET: Specifications
        public async Task<ActionResult> Index(int productId)
        {
            var product = db.Products.FindAsync(productId);
            db.Specifications.Include(nameof(s.SpecificationType));
            var specifications = from s in db.Specifications
                                 where s.ProductId == productId
                                 select s;
            var viewModel = new SpecificationIndexViewModel()
            {
                Product = await product,
                Specifications = await specifications.ToListAsync()
            };
            return View(viewModel);
        }

        // GET: Specifications/Details/5
        public async Task<ActionResult> Details(int productId, int? id, string backUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.Specifications
                .Include(nameof(s.Product))
                .Include(nameof(s.SpecificationType));
            Specification specification = await db.Specifications.FindAsync(id);
            if (specification == null)
            {
                return HttpNotFound();
            }
            var viewModel = new SpecificationWithBackUrlViewModel()
            {
                Specification = specification,
                BackUrl = backUrl
            };
            return View(viewModel);
        }

        // GET: Specifications/Create
        public async Task<ActionResult> Create(int productId, string backUrl)
        {
            try
            {
                var (product, selectList) = await GetProductAndSpecificationTypeSelectList(productId);
                var specification = new Specification() { ProductId = productId };
                var specificationEditViewModel = new SpecificationEditViewModel()
                {
                    Product = product,
                    Specification = specification,
                    SpecificationTypes = selectList,
                    BackUrl = backUrl
                };
                return View(specificationEditViewModel);
            }
            catch (ObjectNotFoundException ex)
            {
                return HttpNotFound(ex.Message);
            }
            

        }

        // POST: Specifications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(int productId, [Bind(Include = "SpecificationId,Value,ProductId,SpecificationTypeId")] Specification specification, string backUrl)
        {
            if (ModelState.IsValid)
            {
                db.Specifications.Add(specification);
                await db.SaveChangesAsync();
                if(backUrl == null)
                    return RedirectToSpecificationList(productId);
                else
                    return Redirect(backUrl);
            }
            try
            {
                var (product, selectList) = await GetProductAndSpecificationTypeSelectList(productId, specification.SpecificationTypeId);
                var specificationEditViewModel = new SpecificationEditViewModel()
                {
                    Product = product,
                    Specification = specification,
                    SpecificationTypes = selectList,
                    BackUrl = backUrl
                };
                return View(specificationEditViewModel);
            }
            catch (ObjectNotFoundException ex)
            {
                return HttpNotFound(ex.Message);
            }
        }

        // GET: Specifications/Edit/5
        public async Task<ActionResult> Edit(int productId, int? id, string backUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Specification specification = await db.Specifications.FindAsync(id);
            if (specification == null)
            {
                return HttpNotFound();
            }
            try
            {
                var (product, selectList) = await GetProductAndSpecificationTypeSelectList(productId, specification.SpecificationTypeId);
                var specificationEditViewModel = new SpecificationEditViewModel()
                {
                    Product = product,
                    Specification = specification,
                    SpecificationTypes = selectList,
                    BackUrl = backUrl,
                };
                return View(specificationEditViewModel);
            }
            catch (ObjectNotFoundException ex)
            {
                return HttpNotFound(ex.Message);
            }
        }

        // POST: Specifications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int productId, [Bind(Include = "SpecificationId,Value,ProductId,SpecificationTypeId")] Specification specification, string backUrl)
        {
            if (ModelState.IsValid)
            {
                db.ChangeEntryState(specification, EntityState.Modified);
                await db.SaveChangesAsync();
                if (backUrl == null)
                    return RedirectToSpecificationList(productId);
                else
                    return Redirect(backUrl);
            }
            try
            {
                var (product, selectList) = await GetProductAndSpecificationTypeSelectList(productId, specification.SpecificationTypeId);
                var specificationEditViewModel = new SpecificationEditViewModel()
                {
                    Product = product,
                    Specification = specification,
                    SpecificationTypes = selectList
                };
                return View(specificationEditViewModel);
            }
            catch (ObjectNotFoundException ex)
            {
                return HttpNotFound(ex.Message);
            }
        }

        // GET: Specifications/Delete/5
        public async Task<ActionResult> Delete(int? id, string backUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.Specifications
                .Include(nameof(s.Product))
                .Include(nameof(s.SpecificationType));
            Specification specification = await db.Specifications.FindAsync(id);
            if (specification == null)
            {
                return HttpNotFound();
            }
            var viewModel = new SpecificationWithBackUrlViewModel()
            {
                Specification = specification,
                BackUrl = backUrl
            };
            return View(viewModel);
        }

        // POST: Specifications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int productId, int id, string backUrl)
        {
            Specification specification = await db.Specifications.FindAsync(id);
            if (specification != null)
            {
                db.Specifications.Remove(specification);
                await db.SaveChangesAsync(); 
            }
            if (backUrl == null)
                return RedirectToSpecificationList(productId);
            else
                return Redirect(backUrl);
        }
        [NonAction]
        private async Task<(Product product, SelectList specificationTypes)> GetProductAndSpecificationTypeSelectList(int productId, int? specificationTypeId = null)
        {
            db.Products.Include($"{nameof(p.Category)}.{nameof(c.SpecificationTypes)}");
            var product = await db.Products.FindAsync(productId);
            if (product == null)
                throw new ObjectNotFoundException($"Not found product with id:{productId}");

            SelectList selectList;
            var commonSpecificationTypesQuery = from st in db.SpecificationTypes
                                           where st.Categories.Count == 0
                                           select st;
            var commonSpecificationTypes = await commonSpecificationTypesQuery.ToListAsync();
            if (product.Category?.SpecificationTypes.Count > 0 || commonSpecificationTypes.Count > 0)
            {
                var valueField = nameof(st.SpecificationTypeId);
                var textField = nameof(st.Name);
                IEnumerable<SpecificationType> specificationTypes;
                if (product.Category != null && product.Category.SpecificationTypes != null)
                    specificationTypes = product.Category.SpecificationTypes.Union(commonSpecificationTypes);
                else
                    specificationTypes = commonSpecificationTypes;
                
                if (specificationTypeId.HasValue)
                    selectList = new SelectList(specificationTypes, valueField, textField, specificationTypeId.Value);
                else
                    selectList = new SelectList(specificationTypes, valueField, textField);
            }
            else selectList = null;

            return (product, selectList);

        }
        protected RedirectToRouteResult RedirectToSpecificationList(int productId)
        {
            return RedirectToRoute(RouteNames.Specifications,
                    new { action = "Index", productId });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
