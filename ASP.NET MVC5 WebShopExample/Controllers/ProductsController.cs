﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASP.NET_MVC5_WebShopExample.Models;
using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using System.Data.Entity.Infrastructure;
using ASP.NET_MVC5_WebShopExample.Models.ViewModels;

namespace ASP.NET_MVC5_WebShopExample.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private ApplicationDbContext db;
        public Category C { get; } = new Category();
        public Product P { get; } = new Product();
        public Specification S { get; } = new Specification();
        public SpecificationType ST { get; } = new SpecificationType();

        public ICurrentDateTime CurrentDateTime { get; }

        public ProductsController(ApplicationDbContext dbContext, ICurrentDateTime currentDateTime)
        {
            db = dbContext;
            CurrentDateTime = currentDateTime;
        }

        // GET: Products
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            var products = db.Products
                .Include(nameof(P.Category))
                .Include(nameof(P.CreatedByUser));
            return View(await products.ToListAsync());
        }

        // GET: Products/Details/5
        [AllowAnonymous]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public async Task<ActionResult> Create()
        {
            var categories = await db.Categories.ToListAsync();
            var viewModel = new ProductCreateViewModel()
            {
                Product = new Product(),
                SelectListOfCategories = new SelectList(categories, nameof(C.CategoryId), nameof(C.Name)),
            };
            return View(viewModel);
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ICurrentUser currentUser, [Bind(Include = "ProductId,Name,Description,Cost,CategoryId")] Product product)
        {
            product.CreatedDate = CurrentDateTime.Get();
            product.CreatedByUserId = currentUser.GetUserId();
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                await db.SaveChangesAsync();
                return RedirectToAction("CreateSpecifications", new { id = product.ProductId });
            }

            var viewModel = new ProductCreateViewModel()
            {
                Product = product,
                SelectListOfCategories = new SelectList(await db.Categories.ToListAsync(), nameof(C.CategoryId), nameof(C.Name), product.CategoryId)
            };
            return View(viewModel);
        }


        public async Task<ActionResult> CreateSpecifications(int id)
        {
            var viewModel = await GetViewModelForCreateSpecificaitons(id);

            if (viewModel.Product == null)
                return HttpNotFound();

            return View(viewModel);
        }
        [HttpPost]
        public async Task<ActionResult> CreateSpecifications([Bind(Include = "Value, SpecificationTypeId, ProductId")] Specification specification)
        {
            if (ModelState.IsValid)
            {
                db.Specifications.Add(specification);
                await db.SaveChangesAsync();
            }

            var viewModel = await GetViewModelForCreateSpecificaitons(specification.ProductId);

            if (viewModel.Product == null)
                return HttpNotFound();

            return View(viewModel);
        }

        [HttpGet]
        public async Task<ActionResult> UploadImages(int id)
        {
            var (product, images) = await GetProductWithInfoForLoadImages(id);

            if (product == null)
            {
                return HttpNotFound();
            }

            return View(new ProductCreateImagesViewModel()
            {
                Product = product,
                Images = images,
            });
        }
        private async Task<(Product product, IEnumerable<ImageLoadViewModel> images)> GetProductWithInfoForLoadImages(int productId)
        {
            var productWithImagesQuery =  from p in db.Products
                                          join i in db.Images on p.ProductId equals i.ProductId into gj
                                          where p.ProductId == productId
                                          from image in gj.DefaultIfEmpty()
                                          select new
                                          {
                                              Product = p,
                                              Image = image == null ? null : new ImageLoadViewModel()
                                              {
                                                  ImageId = image.ImageId,
                                                  Name = image.Name,
                                                  Description = image.Description,
                                                  Width = image.Width,
                                                  Height = image.Height,
                                              }
                                          };
            var productWithImages = await productWithImagesQuery.ToListAsync();

            var product = productWithImages.First().Product;
            var images = productWithImages.Where(pwi => pwi.Image != null).Select(pwi => pwi.Image);
            return (product, images);
        }

        // GET: Products/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            var selectList = new SelectList(await db.Categories.ToListAsync(), nameof(C.CategoryId), nameof(C.Name), product.CategoryId);
            var images = product.Images.
                Select(i => new ImageLoadViewModel()
                {
                    ImageId = i.ImageId,
                    Name = i.Name,
                    Description = i.Description,
                    Width = i.Width,
                    Height = i.Height
                });
            var viewModel = new ProductEditViewModel()
            {
                Product = product,
                SelectListOfCategories = selectList,
                Images = images
            };            
            return View(viewModel);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPost(int productId)
        {
            string[] includeProperties = new string[] { "Name", "Description", "Cost", "RowVersion", "CategoryId" };
            var product = await db.Products.FindAsync(productId);
            product.LastUpdateDate = CurrentDateTime.Get();
            if (TryUpdateModel(product, "Product", includeProperties))
            {
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var selectList = new SelectList(await db.Categories.ToListAsync(), nameof(C.CategoryId), nameof(C.Name), product.CategoryId);
            var images = product.Images.
                Select(i => new ImageLoadViewModel()
                {
                    ImageId = i.ImageId,
                    Name = i.Name,
                    Description = i.Description,
                    Width = i.Width,
                    Height = i.Height
                });
            var viewModel = new ProductEditViewModel()
            {
                Product = product,
                SelectListOfCategories = selectList,
                Images = images
            };            
            return View(viewModel);
        }

        // GET: Products/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Product product = await db.Products.FindAsync(id);
            db.Products.Remove(product);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private async Task<ProductCreateSpecificationsViewModel> GetViewModelForCreateSpecificaitons(int productId)
        {
            var product = await db.Products
                .Include($"{nameof(P.Specifications)}")
                .Include($"{nameof(P.Category)}")
                .Include($"{nameof(P.Category)}.{nameof(C.SpecificationTypes)}")
                .Where(p => p.ProductId == productId)
                .SingleAsync();
            var commonSpecificationTypes = await (from st in db.SpecificationTypes
                                            where st.Categories.Count == 0
                                            select st).ToListAsync();
            IEnumerable<SpecificationType> availableSpecificationTypes;
            if (product != null)
            {
                var categorySpecificationTypes = product.Category.SpecificationTypes;
                availableSpecificationTypes = categorySpecificationTypes.Union(commonSpecificationTypes);
            }
            else
                availableSpecificationTypes = commonSpecificationTypes;

            return new ProductCreateSpecificationsViewModel()
            {
                Product = product,
                SelectListSpecificationTypes = new SelectList(availableSpecificationTypes, nameof(ST.SpecificationTypeId), nameof(ST.Name))
            };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
