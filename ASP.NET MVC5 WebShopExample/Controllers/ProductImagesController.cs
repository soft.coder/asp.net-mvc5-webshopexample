﻿using ASP.NET_MVC5_WebShopExample.AppCode.Services;
using ASP.NET_MVC5_WebShopExample.Models;
using ASP.NET_MVC5_WebShopExample.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_MVC5_WebShopExample.Controllers
{
    public class ProductImagesController : ImagesController
    {
        public ProductImagesController(ICurrentDateTime currentDateTime) : base(currentDateTime)
        {
        }

        // GET: ProductImages
        protected override Task<List<ImageLoadViewModel>> GetLoadInfoOfOwnerImages(int ownerId)
        {
            var images = db.Products
                .Where(p => p.ProductId == ownerId)
                .Join(db.Images, p => p.ProductId, i => i.ProductId,
                    (p, i) => new ImageLoadViewModel() 
                    { 
                        ImageId = i.ImageId, 
                        Name = i.Name, 
                        Description = i.Description,
                        Width = i.Width,
                        Height = i.Height,
                    })
                .ToListAsync();
            return images;
        }
        protected override Image GetOwnerImage(int ownerId, int id)
        {
            // prevent load data column from db
            var imageQuery = (from p in db.Products
                    join i in db.Images on p.ProductId equals i.ProductId
                    where i.ImageId == id && p.ProductId == ownerId
                    select new {
                         i.ImageId,
                         i.Name,
                         i.Description,
                         i.Width,
                         i.Height,
                         i.UploadedDate,
                         i.UploadedByUserId,
                         UploadedEmail = i.UploadedByUser.Email,
                         i.ProductId,
                    });
            var image = imageQuery.Single();
            return new Image()
            {
                ImageId = image.ImageId,
                Name = image.Name,
                Description = image.Description,
                Width = image.Width,
                Height = image.Height,
                UploadedDate = image.UploadedDate,
                UploadedByUserId = image.UploadedByUserId,
                UploadedByUser = new ApplicationUser()
                {
                    Email = image.UploadedEmail,
                },
                ProductId = image.ProductId
            };
        }

        protected override void SetImageOwner(Image image, int ownerId)
        {
            image.ProductId = ownerId;
        }


    }
}