﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASP.NET_MVC5_WebShopExample.Models;
using ASP.NET_MVC5_WebShopExample.Models.Enums;
using Ninject.Infrastructure.Language;
using System.ComponentModel.DataAnnotations;
using Microsoft.Ajax.Utilities;
using System.Collections.ObjectModel;

namespace ASP.NET_MVC5_WebShopExample.Controllers
{
    public class SpecificationTypesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SpecificationTypes
        private SpecificationType ST { get; } = new SpecificationType();
        private Category C { get; } = new Category();
        private Product P { get; } = new Product();
        private Specification S { get; } = new Specification();

        public async Task<ActionResult> Index()
        {
            db.SpecificationTypes.Include(nameof(ST.Categories));
            return View(await db.SpecificationTypes.ToListAsync());
        }

        // GET: SpecificationTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.SpecificationTypes.Include(nameof(ST.Categories));
            SpecificationType specificationType = await db.SpecificationTypes.FindAsync(id);
            if (specificationType == null)
            {
                return HttpNotFound();
            }
            return View(specificationType);
        }

        // GET: SpecificationTypes/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Categories = new MultiSelectList(await db.Categories.ToListAsync(), nameof(C.CategoryId), nameof(C.Name));
            ViewBag.SpecificationFilters = new SelectList(Enum.GetNames(typeof(SpecificationFilter)));
            return View();
        }

        // POST: SpecificationTypes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name,Unit,Filter, Categories")] SpecificationType specificationType, IEnumerable<int> selectedCategoryIds)
        {
            ICollection<Category> selectedCategories;
            if (selectedCategoryIds != null)
                selectedCategories = await db.Categories
                    .Where(c => selectedCategoryIds.Contains(c.CategoryId))
                    .ToListAsync();
            else 
                selectedCategories = new Collection<Category>();

            if (ModelState.IsValid)
            {
                specificationType.Categories = selectedCategories;
                db.SpecificationTypes.Add(specificationType);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Categories = new MultiSelectList(await db.Categories.ToListAsync(), nameof(C.CategoryId), nameof(C.Name));
            ViewBag.SpecificationFilters = new SelectList(Enum.GetNames(typeof(SpecificationFilter)));
            return View(specificationType);
        }

        // GET: SpecificationTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            db.SpecificationTypes.Include(nameof(ST.Categories));
            SpecificationType specificationType = await db.SpecificationTypes.FindAsync(id);
            if (specificationType == null)
            {
                return HttpNotFound();
            }
            var selectedCategoryIds = specificationType.Categories.Select(c => c.CategoryId).ToArray();
            ViewBag.Categories = new MultiSelectList(await db.Categories.ToListAsync(), nameof(C.CategoryId), nameof(C.Name), selectedCategoryIds) ;
            ViewBag.SpecificationFilters = new SelectList(Enum.GetNames(typeof(SpecificationFilter)));
            return View(specificationType);
        }

        // POST: SpecificationTypes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<ActionResult> Edit([Bind(Include = "SpecificationTypeId,Name,Unit,Filter")] SpecificationType specificationType, IEnumerable<int> selectedCategoryIds, bool? isContinueSaving)
        {
            List<Category> selectedCategories;
            if (selectedCategoryIds != null)
                selectedCategories = await db.Categories
                    .Where(c => selectedCategoryIds.Contains(c.CategoryId))
                    .ToListAsync();
            else
                selectedCategories = new List<Category>();
            
            db.Attach(db.SpecificationTypes, specificationType);
            var state = db.GetEntry(specificationType).State;
            db.ChangeEntryState(specificationType, EntityState.Modified);
            await db.CollectionLoadAsync(specificationType, st => st.Categories);

            List<Category> usedCommonCategories;
            if (specificationType.Categories.Count == 0 && selectedCategories.Count > 0)
            {
                usedCommonCategories = await db.Categories
                    .Where(c => !selectedCategoryIds.Contains(c.CategoryId) && 
                        c.Products.Where(p => 
                                p.Specifications.Where(
                                        s => s.SpecificationTypeId == specificationType.SpecificationTypeId).Count() > 0
                            ).Count() > 0
                    ).ToListAsync();
            }
            else usedCommonCategories = new List<Category>();

            selectedCategories
                .Except(specificationType.Categories)
                .ForEach(c => specificationType.Categories.Add(c));

            var categoriesForRemove = specificationType.Categories
                .Except(selectedCategories);
            
            List<Category> usedDisabledCategories;
            if (selectedCategoryIds != null && selectedCategoryIds.Count() > 0)
                usedDisabledCategories = categoriesForRemove.Where(
                        c => c.Products.Where(
                            p => p.Specifications.Where(
                                s => s.SpecificationTypeId == specificationType.SpecificationTypeId).Count() > 0
                        ).Count() > 0
                ).ToList();
            else usedDisabledCategories = new List<Category>();

            var usedInCategories = usedCommonCategories.Union(usedDisabledCategories).ToList();

            if(usedInCategories.Count == 0 || (isContinueSaving.HasValue && isContinueSaving.Value))
            {
                categoriesForRemove.ToList().ForEach(c => specificationType.Categories.Remove(c));
                if(usedInCategories.Count > 0  && isContinueSaving.Value)
                {
                    db.Categories.Include($"{nameof(C.Products)}.{nameof(P.Specifications)}.{nameof(S.SpecificationType)}");
                    var usedCategoryIds = usedInCategories.Select(uc => uc.CategoryId);
                    var usedCategoriesWithSpecificationTypes = (from c in db.Categories
                                              where usedCategoryIds.Contains(c.CategoryId)
                                              select c).ToList();

                    foreach(var usedCategory in usedCategoriesWithSpecificationTypes)
                        foreach(var usedProduct in usedCategory.Products)
                            foreach(var usedSpecification in usedProduct.Specifications.ToList())
                                if(usedSpecification.SpecificationTypeId == specificationType.SpecificationTypeId)
                                    db.Specifications.Remove(usedSpecification);
                }
                if (ModelState.IsValid)
                {
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
            }

            var allCategories = await db.Categories.ToListAsync();
            ViewBag.Categories = new MultiSelectList(allCategories, nameof(C.CategoryId), nameof(C.Name), selectedCategoryIds);
            if(usedInCategories.Count > 0)
            {
                db.Categories.Include($"{nameof(C.Products)}.{nameof(P.Specifications)}.{nameof(S.SpecificationType)}");
                var usedCategoryIds = usedInCategories.Select(uc => uc.CategoryId);
                ViewBag.UsedCategories = (from c in db.Categories
                                         where usedCategoryIds.Contains(c.CategoryId)
                                         select c).ToList();
                ViewBag.selectedCategoryIds = selectedCategoryIds;
            }
            ViewBag.SpecificationFilters = new SelectList(Enum.GetNames(typeof(SpecificationFilter)));
            return View(specificationType);
        }

        // GET: SpecificationTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpecificationType specificationType = await db.SpecificationTypes.FindAsync(id);
            if (specificationType == null)
            {
                return HttpNotFound();
            }
            return View(specificationType);
        }

        // POST: SpecificationTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            SpecificationType specificationType = await db.SpecificationTypes.FindAsync(id);
            if (specificationType != null)
            {
                db.SpecificationTypes.Remove(specificationType);
                await db.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
